#ifndef CAMERA_IMPLEMENTATION_H
#define CAMERA_IMPLEMENTATION_H
#include <opencv2/aruco.hpp>
#include <opencv2/core.hpp>
#include <librealsense2/rs.hpp>
#include "PTRealsense2.h"
#include <opencv2/imgproc.hpp>
#include "t265.h"
#include <QtCore>

#include <opencv2/opencv.hpp>   // Include OpenCV API
#include <opencv2/highgui.hpp>

class CAMERA_IMPLEMENTATION : public QThread
{
public:
    struct ARUCO{
        int vector_pos;
        cv::Point2i PTL1;
        cv::Point2i PTR2;
        cv::Point2i PTR3;
        cv::Point2i PTL4;
        std::string id;
        cv::Point3f PTL13d;
        cv::Point3f PTR23d;
        cv::Point3f PTL43d;
        cv::Point3f PTR33d;
        std::vector<cv::Point2i> left_margin; //punti vertici aruco 2d da camera
        std::vector<cv::Point2i> right_margin;
        std::vector<cv::Point2i> AP_LM; //all point left margin
        std::vector<cv::Point2i> AP_RM;
        cv::Point3d left_depth_margin;
        cv::Point3d right_depth_margin;
        std::vector<cv::Point3d> vertici_aruco_depth;
        double lato_alto_marker;
        std::vector<double> lato_misure;
        double media_lunghezza;
        std::vector<cv::Point2i> ALL_POINTS_MARKER;
        std::vector<cv::Point2d> ALL_POINTS_MARKER_3D;
        std::vector<double> rvec;
        std::vector<double>tvec;
        cv::Point2d stereo_pose_extimation;
        double stereo_asset_extimation;



      //  void calibrate_camera_aruco(cv::Mat camera_matrix, cv::Mat dist_coeff);





        // cv::Point3f PTL33d;


        int right_wall;
        int left_wall;



    };

    std::vector<ARUCO> MARKERS;

    bool aruco_on_frame;

    CAMERA_IMPLEMENTATION();
    void D435stream();
    void Aruco();
    void D4353Dinfo();
    double variance(std::vector<double> observations);
    double scarto_quadratico(double valore, double media);

    double Dmedia(std::vector<double> observations);
    double media_mobile(std::vector<double> observations, int n_width);
    double scarto_medio(std::vector<double> observations);
    std::vector<double> moving_avarege_lenght_control(std::vector<double> observations);


    std::vector<cv::Point3d>  all_point_MARKER(cv::Mat depth_point_mat , std::vector<cv::Point3d> marker_four_corner);
    void PublishDepthPoint();
    std::vector<CAMERA_IMPLEMENTATION::ARUCO> ARUCO_depth_extraction(cv::Mat image_of_depth, std::vector<ARUCO> Vector_of_marker, cv::Mat DP_mat);
    void InizializeD435();
    void run();
    std::vector<CAMERA_IMPLEMENTATION::ARUCO> ARUCO_upload(std::vector<ARUCO> Vector_of_marker);
    std::vector<CAMERA_IMPLEMENTATION::ARUCO> all_marker_point_extraction(cv::Mat color_mat, std::vector<ARUCO> Vector_of_marker);
     std::vector<CAMERA_IMPLEMENTATION::ARUCO>extract_mask_point(cv::Mat input_image, std::vector<ARUCO>);

     cv::Vec3f rotationMatrixToEulerAngles(cv::Mat &R);
     bool isRotationMatrix(cv::Mat &R);

    PTRealsense2 D435;
    std::vector<cv::Point2i> nuvola;

    cv::Mat depthMat, colorMat, depthOverColorMap, irMat;
    cv::Mat cameraMatrix;
    cv::Mat PartialColorMat;
    cv::Point2f pt1;
    std::vector<cv::Point3f> POINTCLOUD;
    int rr;
    int cc;

    int mapW = 2000;
    int mapH = 2000;

    cv::Mat MAP;

    float Lvl = 10.0;
    float fascia = 0.05;
    cv::Mat colorMat1;
    double rwscale;
    double clscale;
    //T265 T265cam;
    cv::Point trj_local[10000];
    int n = 0;
    float depth_scale = 0.001;

    cv::Mat MAP_def;
private:
    //_____________FILTERS__________________//

    /* Struct that holds a pointer to an rs2_option, we'll store a pointer to a filter this way;
      int_params and float_params: maps from an option supported by the filter, to the variable that controls
      this option;
      string: map from an option supported by the filter, to the corresponding vector of strings that will
      be used by the gui;
      do_filter: a boolean controlled by the user that determines whether to apply the filter.*/
    struct filter_options {
        rs2::options* filter;
        std::map<rs2_option, int> int_params;
        std::map<rs2_option, float> float_params;
        std::map<rs2_option, std::vector<char*> > string;
        bool do_filter;
    };


    /* Decalre filters: decimation, temporal and spatial
      Decimation - reduces depth frame density; Spatial - edge-preserving spatial smoothing;
      Temporal - reduces temporal noise */


    ///copiatura da github______________________________

    rs2::decimation_filter dec_filter;  // Decimation - reduces depth frame density
    rs2::threshold_filter thr_filter;   // Threshold  - removes values outside recommended range
    rs2::spatial_filter spat_filter;    // Spatial    - edge-preserving spatial smoothing
    rs2::temporal_filter temp_filter;   // Temporal   - reduces temporal noise
    bool to_disparity = false;


    rs2::disparity_transform depth_to_disparity;
    rs2::disparity_transform disparity_to_depth;


    cv::Point2d pose_aruco_estimation;
    double angolo_stima_aruco;
    //const std::string disparity_filter_name = "Disparity";







    //      rs2::decimation_filter dec_filter;
    //  rs2::spatial_filter spat_filter;
    //  rs2::temporal_filter temp_filter;

    //   rs2::disparity_transform disp_filter;
    // rs2::disparity_transform depth_to_disparity;
    ///  rs2::disparity_transform disparity_to_depth;

    //  std::vector<filter_options> filters;

    //   void set_defaults(std::vector<filter_options>& filters, const std::vector<rs2_option>& option_names);
    //      bool is_all_integers(float min, float max, float def, float step);
    //  inline bool is_integer(float f);
    rs2::frame filtered;


    //_________________________________________

    //per un corretto allineamento le risoluzioni dei due sensori color and depth DEVONO essere uguali

    int _rbgWidth = 848;
    int _rgbHeight = 480;
    int _depthWidth = 848;
    int _depthHeight = 480;
    /*
      int _rbgWidth = 1280;
      int _rgbHeight = 720;
      int _depthWidth = 1280;
      int _depthHeight = 720;
*/
    cv::Size2i depthRowCol = cv::Size2i(_depthWidth, _depthHeight);

    int depthFPS = 30;
    cv::Size2i colorRowCol  =cv::Size2i(_rbgWidth, _rgbHeight);
    int colorFPS = 30;
    float tempFilterAlpha = 0.5;

    rs2::context ctx;
    cv::Mat colorMatA;

    rs2::config cfg;
    rs2::pipeline pipe;


    cv:: Mat DepthPoint_Mat;


    rs2::config cfg1;
    rs2::pipeline pipe1;
    rs2::pipeline_profile profile1;
    rs2::device device1;

    // depth intrinsics
    rs2_intrinsics depthIntrinsics;
    // color intrinsics
    rs2_intrinsics colorIntrinsics;
    // ir intrinsics
    rs2_intrinsics irIntrinsics;

    // color 2 depth extrinsics
    rs2_extrinsics _c2dExtrinsics;
    rs2_extrinsics _d2cExtrinsics;


    rs2::frameset frames;
    rs2::frameset framesVOT;
    //VOT265
    rs2_pose pose;
    rs2_vector accel_sample;
    rs2_vector gyro_sample;
    const int map_dim = 1000;
    cv::Point trj[10000];//impongo un limite di punti
    cv::Point pt;
    int thickness = 2;
    cv::Point pax;
    cv::Point pay;
    cv::Mat aximage;
    float angle;
    const int l = 50;
    const int zoom = 100;
    int i = 0;
    int detected = 0;
    bool markerFOUNDED;
    std::vector<int> markerIds;
    std::vector<std::vector<cv::Point2f>> markerCorners, rejectedCandidates;











};

#endif // CAMERA_IMPLEMENTATION_H

