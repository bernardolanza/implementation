#include "ZmqUtility.h"


ZmqUtility::ZmqUtility()
{
}


ZmqUtility::~ZmqUtility()
{
}


MiroZmq::ZmqWheelVelocity ZmqUtility::DeserializeVelocity(char *buf, int size)
{

	float *incomingRightVelocity = (float *)&buf[1];
	float *incomingLeftVelocity = (float *)&buf[5];

	/*	char rightVelocity[4];
	float *incomingRightVelocity;

	std::memcpy(rightVelocity, buf + 1, sizeof(rightVelocity));
	incomingRightVelocity = (float *)rightVelocity;

	char leftVelocity[4];
	float *incomingLeftVelocity;

	std::memcpy(leftVelocity, buf + 5, sizeof(leftVelocity));
	incomingLeftVelocity = (float *)leftVelocity;*/

	MiroZmq::ZmqWheelVelocity velocity;
	velocity.SetData(*incomingRightVelocity, *incomingLeftVelocity);

	return velocity;

}

MiroZmq::ZmqEncoderTick ZmqUtility::DeserializeEncoderTIck(char *buf, int size)
{

	int *incomingRightTicks = (int *)&buf[1];
	int *incomingLeftTicks = (int *)&buf[5];

	MiroZmq::ZmqEncoderTick ticks;
	ticks.SetData(*incomingRightTicks, *incomingLeftTicks);

	return ticks;

}

MiroZmq::ZmqAck ZmqUtility::DeserializeAck(char* buf, int size)
{

	bool active = buf[1] != 0;
	int *incomingCounter = (int *)&buf[2];

	MiroZmq::ZmqAck ack;
	ack.SetData(active, *incomingCounter);

	return ack;
}

MiroZmq::ZmqBrake ZmqUtility::DeserializeBrake(char *buf, int size)
{
	bool *incomingBrakeEnable = (bool *)&buf[1];
	bool *incomingBrakeStatus = (bool *)&buf[2];

	/*	bool brakeEnable = buf[1] != 0;
	bool brakeStatus = buf[2] != 0;*/

	MiroZmq::ZmqBrake brake;
	brake.SetData(*incomingBrakeEnable, *incomingBrakeStatus);

	return brake;
}

MiroZmq::ZmqStatus ZmqUtility::DeserializeStatus(char *buf, int size)
{
	MiroZmq::ZmqStatus status;
	int *incomingError = (int*)&buf[5];

	status.SetData(static_cast<MiroZmq::NavigationStatus>(buf[1]), *incomingError);

	return status;
}

#ifdef ZMQ_UTILITY_USE_OPENCV
MiroZmq::ZmqMat ZmqUtility::DeserializeMat(char *buf, int size)
{
	MiroZmq::ZmqMat mat;
	int *incomingRows = (int*)&buf[1];
	int *incomingCols = (int*)&buf[5];
	int *incomingDataType = (int*)&buf[9];

	cv::Mat incomingMat(*incomingRows, *incomingCols, *incomingDataType, &buf[13]);

	mat.SetData(incomingMat);

	return mat;
}
#endif

MiroZmq::Zmq3DPoints ZmqUtility::Deserialize3DPoints(char *buf, int size)
{
	MiroZmq::Zmq3DPoints points;
	int *incomingRows = (int*)&buf[1];
	int *incomingCols = (int*)&buf[5];
	signed short *incomingXPoints = (signed short*)&buf[9];
	signed short *incomingYPoints = (signed short*)&buf[9 + (sizeof(signed short)* (*incomingRows) * (*incomingCols))];
	signed short *incomingZPoints = (signed short*)&buf[9 + (2 * sizeof(signed short)* (*incomingRows) * (*incomingCols))];

	points.SetData(*incomingRows, *incomingCols, incomingXPoints, incomingYPoints, incomingZPoints);

	return points;
}

MiroZmq::Zmq2DPoints ZmqUtility::Deserialize2DPoints(char *buf, int size)
{
	MiroZmq::Zmq2DPoints points;
	int *incomingRows = (int*)&buf[1];
	int *incomingCols = (int*)&buf[5];
	signed short *incomingXPoints = (signed short*)&buf[9];
	signed short *incomingYPoints = (signed short*)&buf[9 + (sizeof(signed short)* (*incomingRows) * (*incomingCols))];

	points.SetData(*incomingRows, *incomingCols, incomingXPoints, incomingYPoints);

	return points;
}

MiroZmq::ZmqPoisPathOld ZmqUtility::DeserializePoisPathsOld(char *buf, int size)
{
	MiroZmq::ZmqPoisPathOld paths;
	int *incomingNPaths = (int*)&buf[1];
	int *incomingNPoints = (int*)&buf[5];
	signed short *incomingIds = (signed short*)&buf[9];
	signed short *incomingXPoints = (signed short*)&buf[9 + (sizeof(signed short)* (*incomingNPaths))];
	signed short *incomingYPoints = (signed short*)&buf[9 + (sizeof(signed short)* (*incomingNPaths) * ((*incomingNPoints) + 1))];
	signed short *incomingZPoints = (signed short*)&buf[9 + (sizeof(signed short)* (*incomingNPaths) * ((2 * (*incomingNPoints)) + 1))];
	unsigned char *incomingBusyPoints = (unsigned char*)&buf[9 + (sizeof(signed short)* (*incomingNPaths) * ((3 * (*incomingNPoints)) + 1))];

	paths.SetData(*incomingNPaths, *incomingNPoints, incomingIds, incomingXPoints, incomingYPoints, incomingZPoints, incomingBusyPoints);

	return paths;
}

MiroZmq::ZmqPoisPath ZmqUtility::DeserializePoisPaths(char *buf, int size)
{
	MiroZmq::ZmqPoisPath paths;
	int *incomingNPaths = (int*)&buf[1];
	signed short *incomingIds = (signed short*)&buf[5];
	signed short *incomingNPoints = (signed short*)&buf[5 + (sizeof(signed short)* (*incomingNPaths))];

	unsigned long nPointsPaths = 0;
	for (int ii = 0; ii < *incomingNPaths; ii++)
	{
		nPointsPaths += incomingNPoints[ii];
	}

	signed short *incomingXPoints = (signed short*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths))];
	signed short *incomingYPoints = (signed short*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths)) + (sizeof(signed short)*nPointsPaths)];
	signed short *incomingZPoints = (signed short*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths)) + (2 * sizeof(signed short)*nPointsPaths)];
	float *incomingTransformation = (float*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths)) + (3 * sizeof(signed short)*nPointsPaths)];
	unsigned char *incomingBusyPoints = (unsigned char*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths)) + (3 * sizeof(signed short)*nPointsPaths) + (sizeof(float) * 16 * *incomingNPaths)];

	paths.SetData(*incomingNPaths, incomingIds, incomingNPoints, incomingXPoints, incomingYPoints, incomingZPoints, incomingTransformation, incomingBusyPoints);

	return paths;
}

void ZmqUtility::DeserializePoisPaths(char *buf, int size, MiroZmq::ZmqPoisPath &paths)
{
	int *incomingNPaths = (int*)&buf[1];
	std::cout << "incomingNPaths " << *incomingNPaths << std::endl;
	signed short *incomingIds = (signed short*)&buf[5];
	signed short *incomingNPoints = (signed short*)&buf[5 + (sizeof(signed short)* (*incomingNPaths))];

	unsigned long nPointsPaths = 0;
	for (int ii = 0; ii < *incomingNPaths; ii++)
	{
		nPointsPaths += incomingNPoints[ii];
	}

	signed short *incomingXPoints = (signed short*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths))];
	signed short *incomingYPoints = (signed short*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths)) + (sizeof(signed short)*nPointsPaths)];
	signed short *incomingZPoints = (signed short*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths)) + (2 * sizeof(signed short)*nPointsPaths)];
	float *incomingTransformation = (float*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths)) + (3 * sizeof(signed short)*nPointsPaths)];
	unsigned char *incomingBusyPoints = (unsigned char*)&buf[5 + (2 * sizeof(signed short)* (*incomingNPaths)) + (3 * sizeof(signed short)*nPointsPaths) + (sizeof(float) * 16 * *incomingNPaths)];

	paths.SetData(*incomingNPaths, incomingIds, incomingNPoints, incomingXPoints, incomingYPoints, incomingZPoints, incomingTransformation, incomingBusyPoints);

}

MiroZmq::ZmqEigenFloatMatrix ZmqUtility::DeserializeFloatMatrix(char *buf, int size)
{
	MiroZmq::ZmqEigenFloatMatrix matrix;
	int *incomingRows = (int*)&buf[1];
	int *incomingCols = (int*)&buf[5];
	int *elementSize = (int*)&buf[9];
	float *incomingData = (float*)&buf[13];

	Eigen::MatrixXf incomingMatrix(*incomingRows, *incomingCols);
	for (int ii = 0; ii < *incomingRows; ii++)
	{
		for (int jj = 0; jj < *incomingCols; jj++)
		{
			incomingMatrix(jj, ii) = incomingData[(ii*(*incomingCols)) + jj];
		}
	}
	matrix.SetData(incomingMatrix);
	std::cout << "!!!!!!!!!!!!!!!!!!  MATRIX \n" << incomingMatrix << std::endl;
	std::cout << std::endl;

	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._rows << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._cols << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._data[0] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 2:: " << matrix._data[1] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 3:: " << matrix._data[2] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 4:: " << matrix._data[3] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 5:: " << matrix._data[4] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 6:: " << matrix._data[5] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 7:: " << matrix._data[6] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 8:: " << matrix._data[7] << std::endl;

	return matrix;
}

void ZmqUtility::DeserializeFloatMatrix(char *buf, int size, MiroZmq::ZmqEigenFloatMatrix &matrix)
{
	int *incomingRows = (int*)&buf[1];
	int *incomingCols = (int*)&buf[5];
	int *elementSize = (int*)&buf[9];
	float *incomingData = (float*)&buf[13];

	Eigen::MatrixXf incomingMatrix(*incomingRows, *incomingCols);
	for (int ii = 0; ii < *incomingRows; ii++)
	{
		for (int jj = 0; jj < *incomingCols; jj++)
		{
			incomingMatrix(jj, ii) = incomingData[(ii*(*incomingCols)) + jj];
		}
	}
	matrix.SetData(incomingMatrix);
	/*std::cout << "!!!!!!!!!!!!!!!!!!  MATRIX \n" << incomingMatrix << std::endl;
	std::cout << std::endl;

	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._rows << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._cols << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._data[0] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 2:: " << matrix._data[1] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 3:: " << matrix._data[2] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 4:: " << matrix._data[3] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 5:: " << matrix._data[4] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 6:: " << matrix._data[5] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 7:: " << matrix._data[6] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 8:: " << matrix._data[7] << std::endl;*/

}

MiroZmq::ZmqEigenMatrix3f ZmqUtility::DeserializeMatrix3f(char *buf, int size)
{
	MiroZmq::ZmqEigenMatrix3f matrix;
	int *incomingRows = (int*)&buf[1];
	int *incomingCols = (int*)&buf[5];
	int *elementSize = (int*)&buf[9];
	float *incomingData = (float*)&buf[13];

	Eigen::Matrix3f incomingMatrix;
	for (int ii = 0; ii < *incomingRows; ii++)
	{
		for (int jj = 0; jj < *incomingCols; jj++)
		{
			incomingMatrix(jj, ii) = incomingData[(ii*(*incomingCols)) + jj];
		}
	}
	matrix.SetData(incomingMatrix);
	std::cout << "!!!!!!!!!!!!!!!!!!  MATRIX \n" << incomingMatrix << std::endl;
	std::cout << std::endl;

	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._rows << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._cols << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._data[0] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 2:: " << matrix._data[1] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 3:: " << matrix._data[2] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 4:: " << matrix._data[3] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 5:: " << matrix._data[4] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 6:: " << matrix._data[5] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 7:: " << matrix._data[6] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 8:: " << matrix._data[7] << std::endl;

	return matrix;
}

void ZmqUtility::DeserializeMatrix4f(char *buf, int size, MiroZmq::ZmqEigenMatrix4f &matrix)
{
	int *incomingRows = (int*)&buf[1];
	int *incomingCols = (int*)&buf[5];
	int *elementSize = (int*)&buf[9];
	float *incomingData = (float*)&buf[13];

	Eigen::Matrix4f incomingMatrix;
	for (int ii = 0; ii < *incomingRows; ii++)
	{
		for (int jj = 0; jj < *incomingCols; jj++)
		{
			incomingMatrix(jj, ii) = incomingData[(ii*(*incomingCols)) + jj];
		}
	}
	matrix.SetData(incomingMatrix);
	std::cout << "!!!!!!!!!!!!!!!!!!  MATRIX \n" << incomingMatrix << std::endl;
	std::cout << std::endl;

	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._rows << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._cols << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 1:: " << matrix._data[0] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 2:: " << matrix._data[1] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 3:: " << matrix._data[2] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 4:: " << matrix._data[3] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 5:: " << matrix._data[4] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 6:: " << matrix._data[5] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 7:: " << matrix._data[6] << std::endl;
	std::cout << "!!!!!!!!!!!!!!!!!!  COVARIANCE 8:: " << matrix._data[7] << std::endl;

}

MiroZmq::ZmqPose ZmqUtility::DeserializePose(char *buf, int size)
{
	MiroZmq::ZmqPose pose;
	float *incomingX = (float*)&buf[1];
	float *incomingY = (float*)&buf[5];
	float *incomingTheta = (float*)&buf[9];

	pose.SetData(*incomingX, *incomingY, *incomingTheta);

	return pose;
}

void ZmqUtility::DeserializeTilt(char *buf, int size, MiroZmq::ZmqTilt &tilt)
{
	int *incomingTiltType = (int*)&buf[1];

	tilt.SetData(*incomingTiltType);
}




int ZmqUtility::SerializeVelocity(MiroZmq::ZmqWheelVelocity velocity, char* outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &velocity._type, sizeof(unsigned char));			// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &velocity._rightSpeed, sizeof(float));			// right wheel velocity [m/s]
	size += sizeof(float);

	std::memcpy(outBuf + size, &velocity._leftSpeed, sizeof(float));			// left wheel velocity [m/s]
	size += sizeof(float);

	return size;
}

int ZmqUtility::SerializeEncoderTick(MiroZmq::ZmqEncoderTick ticks, char* outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &ticks._type, sizeof(unsigned char));			// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &ticks._rightTick, sizeof(int));			// right ticks
	size += sizeof(int);

	std::memcpy(outBuf + size, &ticks._leftTick, sizeof(int));			// left ticks
	size += sizeof(int);

	return size;
}

int ZmqUtility::SerializeAck(MiroZmq::ZmqAck ack, char* outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &ack._type, sizeof(unsigned char));				// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &ack._active, sizeof(bool));						// activation of the service
	size += sizeof(bool);

	std::memcpy(outBuf + size, &ack._counter, sizeof(int));						// ack counter
	size += sizeof(int);

	return size;
}

int ZmqUtility::SerializeBrake(MiroZmq::ZmqBrake brake, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &brake._type, sizeof(unsigned char));			// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &brake._brakeEnable, sizeof(bool));				// enable of brakes
	size += sizeof(bool);

	std::memcpy(outBuf + size, &brake._brakeStatus, sizeof(bool));				// status of brakes
	size += sizeof(bool);

	return size;
}

int ZmqUtility::SerializeStatus(MiroZmq::ZmqStatus status, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &status._type, sizeof(unsigned char));			// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &status._status, sizeof(int));					// navigation status (enumerator)
	size += sizeof(int);

	std::memcpy(outBuf + size, &status._param, sizeof(int));					// parm
	size += sizeof(int);

	return size;
}

#ifdef ZMQ_UTILITY_USE_OPENCV
int ZmqUtility::SerializeMat(MiroZmq::ZmqMat mat, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &mat._type, sizeof(unsigned char));				// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &mat._rows, sizeof(int));						// n. of rows
	size += sizeof(int);

	std::memcpy(outBuf + size, &mat._cols, sizeof(int));						// n. of cols
	size += sizeof(int);

	std::memcpy(outBuf + size, &mat._dataType, sizeof(int));					// data type of Mat element
	size += sizeof(int);

	std::memcpy(outBuf + size, mat._data, sizeof(unsigned char)* mat._rows * mat._cols * mat._channels);		// Mat data
	size += (sizeof(unsigned char)* mat._rows * mat._cols * mat._channels);

	return size;
}
#endif

int ZmqUtility::Serialize3DPoints(MiroZmq::Zmq3DPoints points, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &points._type, sizeof(unsigned char));				// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &points._rows, sizeof(int));							// n. of rows
	size += sizeof(int);

	std::memcpy(outBuf + size, &points._cols, sizeof(int));							// n. of cols
	size += sizeof(int);

	std::memcpy(outBuf + size, points._xPoints, sizeof(signed short)* points._rows * points._cols);		// X Point data
	size += (sizeof(signed short)* points._rows * points._cols);

	std::memcpy(outBuf + size, points._yPoints, sizeof(signed short)* points._rows * points._cols);		// X Point data
	size += (sizeof(signed short)* points._rows * points._cols);

	std::memcpy(outBuf + size, points._zPoints, sizeof(signed short)* points._rows * points._cols);		// X Point data
	size += (sizeof(signed short)* points._rows * points._cols);

	return size;
}

int ZmqUtility::Serialize2DPoints(MiroZmq::Zmq2DPoints points, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &points._type, sizeof(unsigned char));				// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &points._rows, sizeof(int));							// n. of rows
	size += sizeof(int);

	std::memcpy(outBuf + size, &points._cols, sizeof(int));							// n. of cols
	size += sizeof(int);

	std::memcpy(outBuf + size, points._xPoints, sizeof(signed short)* points._rows * points._cols);		// X Point data
	size += (sizeof(signed short)* points._rows * points._cols);

	std::memcpy(outBuf + size, points._yPoints, sizeof(signed short)* points._rows * points._cols);		// X Point data
	size += (sizeof(signed short)* points._rows * points._cols);

	return size;
}

int ZmqUtility::SerializePoisPathsOld(MiroZmq::ZmqPoisPathOld paths, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &paths._type, sizeof(unsigned char));										// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &paths._nPaths, sizeof(int));												// n. of rows
	size += sizeof(int);

	std::memcpy(outBuf + size, &paths._nPoints, sizeof(int));												// n. of cols
	size += sizeof(int);

	std::memcpy(outBuf + size, paths._ids, sizeof(signed short)* paths._nPaths);							// Poi ids
	size += sizeof(signed short)* paths._nPaths;

	std::memcpy(outBuf + size, paths._xPoints, sizeof(signed short)* paths._nPaths* paths._nPoints);		// X Point data
	size += sizeof(signed short)* paths._nPaths* paths._nPoints;

	std::memcpy(outBuf + size, paths._yPoints, sizeof(signed short)* paths._nPaths* paths._nPoints);		// Y Point data
	size += sizeof(signed short)* paths._nPaths* paths._nPoints;

	std::memcpy(outBuf + size, paths._zPoints, sizeof(signed short)* paths._nPaths* paths._nPoints);		// Z Point data
	size += sizeof(signed short)* paths._nPaths* paths._nPoints;

	std::memcpy(outBuf + size, paths._busyPoints, sizeof(unsigned char)* paths._nPaths* paths._nPoints);	// Array of flag to say if the points are busy or not
	size += sizeof(unsigned char)* paths._nPaths* paths._nPoints;

	return size;
}

int ZmqUtility::SerializePoisPaths(MiroZmq::ZmqPoisPath paths, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &paths._type, sizeof(unsigned char));										// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &paths._nPaths, sizeof(int));												// n. of rows
	size += sizeof(int);

	std::memcpy(outBuf + size, paths._ids, sizeof(signed short)* paths._nPaths);							// Poi ids
	size += sizeof(signed short)* paths._nPaths;

	std::memcpy(outBuf + size, paths._nPoints, sizeof(signed short)* paths._nPaths);						// N points for each path
	size += sizeof(signed short)* paths._nPaths;


	unsigned long nPointsPaths = 0;
	for (int ii = 0; ii < paths._nPaths; ii++)
	{
		nPointsPaths += paths._nPoints[ii];
	}

	std::memcpy(outBuf + size, paths._xPoints, sizeof(signed short)* nPointsPaths);		// X Point data
	size += sizeof(signed short)* nPointsPaths;

	std::memcpy(outBuf + size, paths._yPoints, sizeof(signed short)* nPointsPaths);		// Y Point data
	size += sizeof(signed short)* nPointsPaths;

	std::memcpy(outBuf + size, paths._zPoints, sizeof(signed short)* nPointsPaths);		// Z Point data
	size += sizeof(signed short)* nPointsPaths;


	std::memcpy(outBuf + size, paths._transformationData, sizeof(float)* paths._nPaths * 16);		// Transformation Matrix
	size += sizeof(float)* paths._nPaths * 16;

	std::memcpy(outBuf + size, paths._busyPoints, sizeof(unsigned char)* paths._nPaths);	// Array of flag to say if the points are busy or not
	size += sizeof(unsigned char)* paths._nPaths;

	return size;
}

int ZmqUtility::SerializeFloatMatrix(MiroZmq::ZmqEigenFloatMatrix matrix, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &matrix._type, sizeof(unsigned char));						// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &matrix._rows, sizeof(int));									// n. of rows
	size += sizeof(int);

	std::memcpy(outBuf + size, &matrix._cols, sizeof(int));									// n. of cols
	size += sizeof(int);

	std::memcpy(outBuf + size, &matrix._elementSize, sizeof(int));							// n. of bytes for each element
	size += sizeof(int);

	std::memcpy(outBuf + size, matrix._data, sizeof(float)* matrix._rows*matrix._cols);		// X Point data
	size += sizeof(float)* matrix._rows*matrix._cols;

	return size;
}

int ZmqUtility::SerializePose(MiroZmq::ZmqPose pose, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &pose._type, sizeof(unsigned char));					// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &pose._x, sizeof(float));							// X pose
	size += sizeof(float);

	std::memcpy(outBuf + size, &pose._y, sizeof(float));							// Y pose
	size += sizeof(float);

	std::memcpy(outBuf + size, &pose._theta, sizeof(float));						// Theta pose
	size += sizeof(float);

	return size;
}

int ZmqUtility::SerializeTilt(MiroZmq::ZmqTilt tilt, char *outBuf)
{
	int size = 0;

	std::memcpy(outBuf + size, &tilt._type, sizeof(unsigned char));			// type of variable
	size += sizeof(unsigned char);

	std::memcpy(outBuf + size, &tilt._tiltType, sizeof(bool));				// enable tilt
	size += sizeof(int);


	return size;
}