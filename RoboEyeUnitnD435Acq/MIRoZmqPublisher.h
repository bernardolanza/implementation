#pragma once

#include <QObject>
#include <iostream>

#include <zmq.hpp>
#include "zhelpers.hpp"

#include "ZmqUtility.h"

#define MAX_MESSAGE_SIZE 10000000

#define VISUALIZE_IMAGE 1
#define DEBUG_MESSAGE 0
#define ZMQ_UTILITY_USE_OPENCV

class MIRoZmqPublisher : public QObject
{
public:
	MIRoZmqPublisher(std::string zmqAddress);
	~MIRoZmqPublisher();

	bool publishAck(MiroZmq::ZmqAck ack2send);
	bool publishString(std::string string2send);
	bool PublishVelocity(MiroZmq::ZmqWheelVelocity velocity2send);
	bool publishStatus(MiroZmq::ZmqStatus status2send);
#ifdef ZMQ_UTILITY_USE_OPENCV
	bool publishRgbFrame(MiroZmq::ZmqMat &mat);
#endif
	bool publish3DPoints(MiroZmq::Zmq3DPoints points2Send);
	bool publish2DPoints(MiroZmq::Zmq2DPoints points2Send);
	bool publishPoisPaths(MiroZmq::ZmqPoisPath points2Send);
	bool publishFloatMatrix(MiroZmq::ZmqEigenFloatMatrix points2Send);
	bool publishPose(MiroZmq::ZmqPose pose2Send);

private:
	zmq::context_t _m_context;
	zmq::socket_t *_m_publisher;

	std::string _m_address;

	char *_m_buffer = new char[MAX_MESSAGE_SIZE];
};

