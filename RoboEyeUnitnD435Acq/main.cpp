#include "RoboeyeUnitnD435Acq.h"
#include "camera_implementation.h"
#include "d435new.h"
#include <QCoreApplication>
#include "t265.h"
#include <QThread>
#include <QMutex>
#include <QMutexLocker>
#include <unistd.h>



int i = 0;


int main(int argc, char *argv[])
{

    QCoreApplication a(argc, argv);


    //RoboeyeUnitnD435Acq _mainD435AcqScript;
    /*
    QMutex mutex2;
    mutex2.lock();

    t265.InizializeT265();
*/

    // QMutex mutex3;
    //   mutex3.lock();
    T265 t265;
    t265.InizializeT265();
    CAMERA_IMPLEMENTATION camere;
    camere.InizializeD435();




    while (true){
        QMutex mutex;
        mutex.lock();

        if (i > 5){





            if (camere.MARKERS.size() != 0){
                if (camere.aruco_on_frame == true){


                    t265.left_depth_aruco = camere.MARKERS[0].left_depth_margin;
                    t265.right_depth_aruco = camere.MARKERS[0].right_depth_margin;
                    t265.all_points_marker_3d = camere.MARKERS[0].ALL_POINTS_MARKER_3D;


                    t265.stereo_pose = camere.MARKERS[0].stereo_pose_extimation;
                    t265.stereo_angle =  camere.MARKERS[0].stereo_asset_extimation;
                  //  std::cout<<"stereo pose D435  all!!" << camere.MARKERS[0].stereo_pose_extimation << std::endl;
                    //std::cout<<"stereo pose T265  all!!" <<  t265.stereo_pose << std::endl;


                }
                else {
                    t265.left_depth_aruco = cv::Point3d(0,0,0);
                    t265.right_depth_aruco = cv::Point3d(0,0,0);


                }
            }
            else {
                std::cout<<"no marker detected"<<std::endl;
            }
        }

        i++;





       // t265.pointcloud = camere.nuvola;

        t265.run();
        camere.run();



        mutex.unlock();
        //sleep(1);

    }




    //t265.GenerateMap();
    //mutex2.unlock();



    //mutex3.unlock();

    //camere.start();


    //inizializing

    //rgb stream + other function
    //camere.D435stream();







    return a.exec();
}
