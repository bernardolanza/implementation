#include "camera_implementation.h"
#include <QtCore>
#include <QDebug>

CAMERA_IMPLEMENTATION::CAMERA_IMPLEMENTATION()
{



}

void CAMERA_IMPLEMENTATION::run()
{

    qDebug() << "running";


    D435stream(); //color stream
    D4353Dinfo(); //depth stream

    PublishDepthPoint(); // depth extraction
    Aruco(); //aruco extraction



}



void CAMERA_IMPLEMENTATION::InizializeD435(){



    std::vector<rs2::sensor> sensors;

    rs2::device_list devices = ctx.query_devices(); // Get a snapshot of currently connected devices





    /*     if (devices.size() == 0)	{
      std::cout<< "No device detected. Is it plugged in?"<<std::endl;
    }
    else if(devices.size() == 1){
        std::cout<< "Solo un dispositivo rilevato, non sono programmato per questo"<<std::endl;

    }
    else {
        std::cout << "found " << devices.size() << " realsense devices." << std::endl;
        for (size_t i = 0; i < devices.size(); i++) {
           rs2::device dev = devices[i];
           std::string devSerialNumber = dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);

          std::cout << "dev " << i << " -> SN= " << devSerialNumber << std::endl;
//identifico il /i sensori della T265 anche se  l inizializz di questa e a parte
           if(devSerialNumber == "852212110836"){
                  for (rs2::sensor sensor : dev.query_sensors()) {
                      auto sensor_stream_profiles = sensor.get_stream_profiles();
                      std::cout<<"T265 stream profile:"<<std::endl;


        }
    }*/


    //____analisi D435 senza le pipeline lavorondo con i sensori sincronizzati in sync


    /*
          else if (devSerialNumber == "913522071562"){


                for (rs2::sensor sensor : dev.query_sensors()) {
                    auto sensor_stream_profiles = sensor.get_stream_profiles();
                    if(rs2::depth_sensor depth_sensor = sensor.as<rs2::depth_sensor>()){
                        std::cout<<"trovato depth sensor"<<std::endl;
                        depth_sensor.open(sensor_stream_profiles[77]); // 640 x 480 60Hz
                        depth_sensor.start(sync);
                        sensors.push_back(sensor);

                }
                    else if(sensor_stream_profiles[0].stream_type() == RS2_STREAM_COLOR){
                        rs2::color_sensor color_sensor = sensor;
                          std::cout<<"trovato color sensor"<<std::endl;
                          sensor.open(sensor_stream_profiles[85]); // 640 x 480 60Hz RGB8

                         sensor.start(sync);
                         sensors.push_back(sensor);
                    }




                }
                }

*/



    //852212110836 -> dev sn t265
    //913522071562 -> dev sn d435

    //D435.initializeSensor(cv::Size2i(_depthWidth, _depthHeight), _depthFPS, cv::Size2i(_rbgWidth, _rgbHeight), _rgbFPS, 0.5, false, devSerialNumber);
    //if (devSerialNumber == "852212110836"){





    //inizializzazione standard di d435

    std::string devSerialNumberD435 = "913522071562";
    //cfg1.enable_device(devSerialNumberD435 );
    //rs2::pipeline pipe1(ctx);


    cfg1.enable_stream(RS2_STREAM_DEPTH, depthRowCol.width, depthRowCol.height, RS2_FORMAT_Z16, depthFPS);

    cfg1.enable_stream(RS2_STREAM_COLOR, colorRowCol.width, colorRowCol.height, RS2_FORMAT_BGR8, colorFPS);

    cfg1.enable_stream(RS2_STREAM_INFRARED, depthRowCol.width, depthRowCol.height, RS2_FORMAT_Y8, depthFPS);



    // std::cout << "D435 enabled" <<  std::endl;
    profile1 = pipe1.start(cfg1);

    device1 = profile1.get_device();

    rs2::sensor depthSensor = device1.first<rs2::depth_sensor>();
    depth_scale = depthSensor.as<rs2::depth_sensor>().get_depth_scale();
    // std::cout << "Depth Scale (mm)= " << depth_scale << std::endl;

    rs2::video_stream_profile irStream = profile1.get_stream(RS2_STREAM_INFRARED).as<rs2::video_stream_profile>();
    irIntrinsics = irStream.get_intrinsics();

    float fovi[2]; // X, Y fov
    rs2_fov(&irIntrinsics, fovi);

    rs2::video_stream_profile depthStream = profile1.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();
    depthIntrinsics = depthStream.get_intrinsics();

    float fovd[2]; // X, Y fov
    rs2_fov(&depthIntrinsics, fovd);



    //_________________FILTERS_______________//

    //constructor


    // Declare disparity transform from depth to disparity and vice versa
    // rs2::disparity_transform depth_to_disparity;
    //rs2::disparity_transform depth_to_disparity(true);




    // initialize a vector that would allow iterating on possible filter options
    ///  std::vector<rs2_option> option_names = { RS2_OPTION_FILTER_MAGNITUDE, RS2_OPTION_FILTER_SMOOTH_ALPHA,
    //       RS2_OPTION_FILTER_SMOOTH_DELTA };

    /*

     // initialize a vector that holds filters and their option values
     filter_options dec_struct;
     dec_struct.filter = &dec_filter;
     filter_options disparity_struct;
     disparity_struct.filter = &depth_to_disparity;
     filter_options spat_struct;
     spat_struct.filter = &spat_filter;
     filter_options temp_struct;
     temp_struct.filter = &temp_filter;
     filters = { dec_struct, disparity_struct, spat_struct, temp_struct };
*/
    // set parameters that are controlled by the user to default values
    //  set_defaults(filters, option_names);

    //  filters[3].float_params[RS2_OPTION_FILTER_SMOOTH_ALPHA] = 0.3;


    // Initialize a vector that holds filters and their options
    //  std::vector<filter_options> filters;

    // The following order of emplacement will dictate the orders in which filters are applied
    //   filters.emplace_back("Decimate", dec_filter);
    //   filters.emplace_back("Threshold", thr_filter);
    //    filters.emplace_back(disparity_filter_name, depth_to_disparity);
    ////   filters.emplace_back("Spatial", spat_filter);
    //   filters.emplace_back("Temporal", temp_filter);





    //_______________________//


    //setto le opzioni per i filtri

    /*

     temp_filter.set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, tempFilterAlpha);
     temp_filter.set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 20);



 std::cout << "Depth smooth filter active with alpha = " << tempFilterAlpha << std::endl;

    spat_filter.set_option(RS2_OPTION_FILTER_MAGNITUDE, 3.0f);
    spat_filter.set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, 0.6f);
    spat_filter.set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 20);
    spat_filter.set_option(RS2_OPTION_HOLES_FILL,4.0f);

    dec_filter.set_option(RS2_OPTION_FILTER_MAGNITUDE, 2.0f);
    temp_filter.set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA,0.4);
    temp_filter.set_option(RS2_OPTION_FILTER_SMOOTH_DELTA,20);

*/

    //____________________________________________//



    rs2::video_stream_profile colorStream = profile1.get_stream(RS2_STREAM_COLOR).as<rs2::video_stream_profile>();
    colorIntrinsics = colorStream.get_intrinsics();

    float fovc[2]; // X, Y fov
    rs2_fov(&colorIntrinsics, fovc);


    _c2dExtrinsics = colorStream.get_extrinsics_to(depthStream);
    _d2cExtrinsics = depthStream.get_extrinsics_to(colorStream);



    std::cout <<"sensors D435 inizialized succesfully"<<std::endl;
    markerFOUNDED = false;
    aruco_on_frame = false;









    //D435stream();
}//foo




void  CAMERA_IMPLEMENTATION::D435stream() {

    //funzione iniz t265 (classe ext)






    //GETCOLOR
    //pipe1.start(cfg1);

    // this->mutex->lock();

    //_______bisogna allineare i frames perche il sensore depth crea un frame shiftato rispetto al color

    rs2::align align_to(RS2_STREAM_COLOR);

    rs2::align align = rs2::align(align_to);


    frames = pipe1.wait_for_frames();

    frames =  align.process(frames);











    // frames = rs2::align.process(frames_not_aligned).As<FrameSet>().DisposeWith(frames_not_aligned);


    rs2::video_frame colorFrame = frames.get_color_frame();

    // check mat dimension
    if (colorMat.rows != colorFrame.get_height() || colorMat.cols != colorFrame.get_width() || colorMat.type() != CV_8UC3) {
        colorMat = cv::Mat(colorFrame.get_height(), colorFrame.get_width(), CV_8UC3);
    }

    // copy data pointer to internal var
    colorMat.data = (uchar*)colorFrame.get_data();

    // std::cout <<"color mat dimension"<< colorMat.rows <<"  " << colorMat.cols << std::endl;

    // std::cout<<"waiting for frame to display"<<std::endl;

    /*
    cv::Mat img2Show;
    cv::imshow("color",colorMat);

     cv::waitKey(1);
*/





















}




void  CAMERA_IMPLEMENTATION::Aruco(){
    //cv::resize(colorMat, colorMatA, cv::Size(848,480));

    //std::cout<< "starting aruco degtection"<<std::endl;

    //   cameraMatrix = (cv::Mat_<double>(3,3) <<  D435._depthIntrinsics.fx, 0, D435._depthIntrinsics.ppx, 0, D435._depthIntrinsics.fy, D435._depthIntrinsics.ppy, 0, 0, 1);
    //  double distCoeff[5] = { D435._depthIntrinsics.coeffs[0],D435._depthIntrinsics.coeffs[1],D435._depthIntrinsics.coeffs[2],D435._depthIntrinsics.coeffs[3],D435._depthIntrinsics.coeffs[4] };

    //Marker detection




    cv::Ptr<cv::aruco::DetectorParameters> parameters = cv::aruco::DetectorParameters::create();
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::aruco::detectMarkers(colorMat, dictionary, markerCorners, markerIds, parameters, rejectedCandidates);
    if (markerCorners.size() == 0){
        aruco_on_frame = false; //bool che mi dice quando c e un aruco sullo schermo
    }
    else
    {
        aruco_on_frame = true;
    }




    cv::Mat cameraMatrix, distCoeffs;
    cameraMatrix = (cv::Mat_<double>(3,3) <<  colorIntrinsics.fx, 0, colorIntrinsics.ppx, 0, colorIntrinsics.fy, colorIntrinsics.ppy, 0, 0, 1);
    distCoeffs = (cv::Mat_<double>(5,1) <<  0, 0, 0, 0, 0);


    // camera parameters are read from somewhere
    // readCameraParameters(cameraMatrix, distCoeffs);
    std::vector<cv::Vec3d> rvecs, tvecs;
    cv::aruco::estimatePoseSingleMarkers(markerCorners, 10.4, cameraMatrix, distCoeffs, rvecs, tvecs);
    if (rvecs.size() != 0 && tvecs.size() !=  0 ){




        cv::Mat rot_mat_sdr_aruco ;
        cv::Rodrigues(rvecs,rot_mat_sdr_aruco);

        cv::Vec3f eulers;
        eulers = rotationMatrixToEulerAngles(rot_mat_sdr_aruco);

        //std::cout<<" r vec  : "<< eulers[0] << "  " <<eulers[1]<< "  " <<eulers[2]<< std::endl;





        pose_aruco_estimation = cv::Point2d(tvecs[0][0],tvecs[0][2]);
        angolo_stima_aruco = double(eulers[1]);


        //tvecs[2] assez profondita positiva
        //sse y verso l alto negatino tvecs[1]
        //assex traslazione laterale


        //rvecs [2] asse y per rotazione planare

    }







    cv::Mat outputImage = colorMat.clone();
    //CREAZIONE STRUTTURA ARUCO (AL MOMENTO SINGOLA MA POTRA GESTIRNE PIU DI UNO)

    if (markerCorners.size() > detected){
        if (aruco_on_frame == true){
            markerFOUNDED = true;
            std::string id = std::to_string(markerIds[detected]);
            std::string nameM = "marker " + id ;
            // std::cout<< "name marker :"<<nameM<<std::endl;
            ARUCO MARKER; //STRUTTURA PER GESTIRE SINGOLO MARKER

            std::cout<< "new marker detected, id : "<<nameM<<std::endl;

            MARKER.vector_pos = detected;


            MARKER.id = std::to_string(markerIds[MARKER.vector_pos]);





            MARKERS.push_back(MARKER); //VETTORE MARKER DIVERSI

            MARKERS = ARUCO_upload(MARKERS); //FUNZIONE CHE CHIAMO OGNI CICLO PER UPLODARE LA STRUTTURA MARKER OGNI FRAME

            detected++;

        }
    }




    if (markerFOUNDED == true){
        if (aruco_on_frame == true){


            MARKERS = ARUCO_upload(MARKERS);//FUNZIONE CHE CHIAMO OGNI CICLO PER UPLODARE LA STRUTTURA MARKER OGNI FRAME
            MARKERS = all_marker_point_extraction(colorMat, MARKERS);// FUNZIONE PER ESTRARRE UNA NUVOLA DI PUNTI AFFINE AL MARKER E AD I SUOI VERTICI


            cv::circle(outputImage,MARKERS[0].right_margin[0],5,cv::Scalar(255,255,0));
            cv::circle(outputImage,MARKERS[0].right_margin[1],5,cv::Scalar(255,255,0));
            // cv::circle(outputImage,MARKERS[0].left_margin[2],5,cv::Scalar(255,255,0));





        }
    }








    //cv::imshow("Display window Camera with marker", outputImage);

    // cv::waitKey(1);


}
void CAMERA_IMPLEMENTATION::D4353Dinfo(){
    //streaming dATI DI PROFONDITÀ

    rs2::depth_frame depthFrame = frames.get_depth_frame();




    // depthFrame = dec_filter.process(depthFrame);
    // depthFrame = thr_filter.process(depthFrame);

    if(to_disparity == false)
    {
        //  depthFrame = depth_to_disparity.process(depthFrame);
        to_disparity = true;
    }


    //depthFrame = spat_filter.process(depthFrame);
    depthFrame = temp_filter.process(depthFrame);


    if(to_disparity == true)
    {
        //  depthFrame = disparity_to_depth.process(depthFrame);
        to_disparity = false;
    }






    // depthFrame = dec_filter.process(depthFrame); //problemi di risoluzione nelle intrinsics
    // depthFrame = depth_to_disparity.process(depthFrame);
    //depthFrame = spat_filter.process(depthFrame);
    depthFrame = temp_filter.process(depthFrame);
    //   depthFrame = disparity_to_depth.process(depthFrame);


    //___________________________________________________________//

    if (depthMat.rows != depthFrame.get_height() || depthMat.cols != depthFrame.get_width() || depthMat.type() != CV_16UC1) {
        depthMat = cv::Mat(depthFrame.get_height(), depthFrame.get_width(), CV_16UC1);
    }
    depthMat.data = (uchar*)depthFrame.get_data();
    // std::cout <<"depth mat dimension"<< depthMat.rows <<"  " << depthMat.cols << std::endl;




    //_______________________FILTERS____________________________//


    /*
    bool do_decimate = filters[0].do_filter;
    bool do_disparity = filters[1].do_filter;
    bool do_spatial = filters[2].do_filter;
    bool do_temporal = filters[3].do_filter;
*/

    /* Apply filters. The implemented flow of the filters pipeline is in the following order: apply decimation filter,
    transform the scence into disparity domain, apply spatial filter, apply temporal filter, revert the results back
    to depth domain (each post processing block is optional and can be applied independantly).
    */


    /*
    if (do_decimate)
            filtered = static_cast<rs2::decimation_filter*>(filters[0].filter)->process(filtered);

        if (do_disparity)
            filtered = static_cast<rs2::disparity_transform*>(filters[1].filter)->process(filtered);
        if (do_spatial)
            filtered = static_cast<rs2::spatial_filter*>(filters[2].filter)->process(filtered);

    if (do_temporal)
    filtered = static_cast<rs2::temporal_filter*>(filters[3].filter)->process(filtered);

    if (do_disparity)
        filtered = disparity_to_depth.process(filtered);
*/
    //___________________________________________________________//


    rs2::video_frame irFrame = frames.get_infrared_frame();

    // check mat dimension
    if (irMat.rows != irFrame.get_height() || irMat.cols != irFrame.get_width() || irMat.type() != CV_8UC1) {
        irMat = cv::Mat(irFrame.get_height(), irFrame.get_width(), CV_8UC1);
    }

    // copy data pointer to internal var
    irMat.data = (uchar*)irFrame.get_data();




}
std::vector<cv::Point3d>   CAMERA_IMPLEMENTATION::all_point_MARKER(cv::Mat depth_point_mat , std::vector<cv::Point3d> marker_four_corner){



}
void CAMERA_IMPLEMENTATION::PublishDepthPoint(){
    //eXTRACT 3D POINTS
    //cv::resize(colorMat, colorMat1, cv::Size(848,480));
    PartialColorMat = colorMat.clone();

    float depthPixel[2];
    float distance;
    float depthPoint[3];
    // cv::Mat MAP = cv::imread("/home/bernardo/BENTESI/RoboEyeUnitnD435Acq_ben extermnal implemention/MAPdef1.jpg");
    cv::Mat MAP(mapW,mapH,CV_8UC3, cv::Scalar(0,0,0));


    //POINTCLOUD FOR T265 MAP

    nuvola.clear();


    //pulizia del vettore punti di profondita del frme vecchio

    //estrazione depth point
    //CREO UNA MATRICE DI POIND3D PER GESTIRE LE INFORMAZIONI DI PROFONDITA
    cv::Mat_<cv::Point3d>  DepthPoint_Mat(depthMat.rows, depthMat.cols);
    cv::Mat colorized (depthMat.rows,depthMat.cols, CV_8UC3);



    for (int r = 0; r < depthMat.rows; r++) {
        for (int c = 0; c < depthMat.cols; c++) {
            depthPixel[0] = (float)c;
            depthPixel[1] = (float)r;


            distance = (float)depthMat.at<uint16_t>(r, c) * depth_scale;


            //  std::cout<<r<<std::endl;

            rs2_deproject_pixel_to_point(depthPoint, &depthIntrinsics, depthPixel, distance);

            cv::Point3d DP = cv::Point3d(depthPoint[0],depthPoint[1],depthPoint[2]);


            //MATRICE POINT3D DI DATI PROFONDITA
            DepthPoint_Mat.at<cv::Point3d>(r,c) = cv::Point3d(depthPoint[0],depthPoint[1],depthPoint[2]);
            //MATRICE DI COLORI BW PER VISUALIZZARE LA PROFONDITA
            colorized.at<cv::Vec3b>(r,c) = cv::Vec3b((int)(depthPoint[2]*255/7),(int)(depthPoint[2]*255/7),(int)(depthPoint[2]*255/7));


            //std::cout<<"depth point map size"<<DepthPoint_Mat.at<cv::Point3d>(r,c)<<std::endl;



            //SELEZIONE FASCIA VISIVA PER PASSARE DATI ALLA MAPPA GLOBALE

            if ( DP.y >( -fascia) && DP.y <  + fascia ){
                //std::cout<<"punto basale"<<std::endl;
                int w = (int)(DP.x*zoom);
                int h = (int)(DP.z*zoom);
                cv::Point2i MPs((int)w+mapW/2,(int)h+mapH/2);
                cv::Point2i MPl(w,h); //questi sono i punti che passo


                // std::cout<<"punto basale"<<MPs<<std::endl;
                //POINTCLOUD DI DATI PROFONDITA ENVIRONMENT AD UNA DATA FASCIA
                nuvola.push_back(MPl);


                cv::circle(MAP,MPs,5,cv::Scalar(0,255,0));


                //  std::cout<<"mat at r,c: "<< r<<" , "<<c<<"DP :   " <<DepthPoint_Mat.at<cv::Point3d>(r,c) <<std::endl;
            }

            //PER VEDERE QUALE FASCIA SI STA GUARDANDO
            else {


                cv::Vec3b & color = PartialColorMat.at<cv::Vec3b>(r,c);


                color[0] = 0;
                color[1] = 0;
                color[2] = 0;


            }

        }
    }





    //estrazione da matrice depth point di informazioni sensibili





    if (markerFOUNDED  == true){

        if (aruco_on_frame == true)
        {
            //questa e la funzione che mi rallenta brutalmente il loop
            MARKERS = ARUCO_depth_extraction(colorized, MARKERS,DepthPoint_Mat);


        }
        else {
            MARKERS[0].left_depth_margin = cv::Point3d(0.0,0.0,0.0);
            MARKERS[0].right_depth_margin = cv::Point3d(0.0,0.0,0.0);
        }
    }


    cv::imshow("colorized", colorized);
    cv::waitKey(1);


    //cv::imshow("partial", PartialColorMat);
    // cv::waitKey(1);














    //MAPPE

    //  std::cout<<"then.."<<std::endl;

    cv::line(MAP,cv::Point(mapW/2,mapH/2),cv::Point(mapW/2,50+mapH/2),cv::Scalar(0,0,255),5);

    cv::line(MAP,cv::Point(mapW/2,mapH/2),cv::Point((mapW/2)-50,mapH/2),cv::Scalar(255,0,0),5);



    //stiamo modificando la matrice
    cv::Mat visualize;
    cv::resize(MAP, visualize, cv::Size(), 0.3, 0.3);
    // cv::imshow("Display map", visualize);

    // cv::imshow("Display window Camera partial", PartialColorMat);
















    //INVIO DATI A MAPA T265
    //T265Aquisition(nuvola);






    //std::cout<<"depth point:"<<depthPoint[0] << "   " << depthPoint[1]<<"  " << depthPoint[2]<<std::endl;

}

//FUNZIONE PER ESTRARRE PUNTI AFFINI AL MARKER

std::vector<CAMERA_IMPLEMENTATION::ARUCO> CAMERA_IMPLEMENTATION::all_marker_point_extraction(cv::Mat color_mat, std::vector<ARUCO> Vector_of_marker){


    ///siamo qui da implementare l estrazione di profondita della faslia margnale del marker e pensare ad un altro sistema di estrazione usando la depth point matrix

    int ini_size_margin_l = Vector_of_marker[0].AP_LM.size();
    int ini_size_margin_r = Vector_of_marker[0].AP_RM.size();

    if ( Vector_of_marker[0].AP_LM.size() != 0 &&  Vector_of_marker[0].AP_RM.size() != 0){
        for (int ii = 0;ii < ini_size_margin_l;ii++){
            Vector_of_marker[0].AP_LM.pop_back();
        }
        for (int in = 0;in < ini_size_margin_r;in++){
            Vector_of_marker[0].AP_RM.pop_back();
        }
    }


    if ( Vector_of_marker[0].ALL_POINTS_MARKER.size() != 0 ){
        int size_AP = Vector_of_marker[0].ALL_POINTS_MARKER.size();
        for (int ii = 0;ii < size_AP;ii++){
            Vector_of_marker[0].ALL_POINTS_MARKER.pop_back();
        }

    }







    cv::Point2i p1 = Vector_of_marker[0].left_margin[0];

    cv::Point2i p4 = Vector_of_marker[0].left_margin[1];

    cv::Point2i p2 = Vector_of_marker[0].right_margin[0];

    cv::Point2i p3 = Vector_of_marker[0].right_margin[1];
    // cv::circle(color_mat,p2,10,cv::Scalar(0,200,200));

    int fascia_punti = 5;

    for(int kk = 0; kk < color_mat.rows ;kk++){
        for(int jj = 0; jj < color_mat.cols; jj++){
            if(p1.x > p4.x){

                /*
                if(jj >= p4.x-fascia_punti && jj <= p1.x+fascia_punti){ //fasce verticali
                    if(kk >= p1.y   && kk<= p4.y ){
                        cv::Point2i p = cv::Point2i(jj,kk);
                        Vector_of_marker[0].AP_LM.push_back(p);

                        cv::circle(color_mat,p,1,cv::Scalar(0,100,100));
                    }
                }*/

                //p4 prendi
                if(jj> p4.x && jj<p2.x){ ///////nuovo
                    if( kk > p1.y && kk < p3.y){
                        cv::Point2i p = cv::Point2i(jj,kk);
                        Vector_of_marker[0].ALL_POINTS_MARKER.push_back(p);
                    }



                }
            }

            else{
                if(jj> p1.x && jj<p3.x){  ///////nuovo
                    if( kk > p2.y && kk < p4.y){
                        cv::Point2i p = cv::Point2i(jj,kk);
                        Vector_of_marker[0].ALL_POINTS_MARKER.push_back(p);
                    }
                }
            }


            /*

            else {
                if(jj >= p1.x-fascia_punti && jj <= p4.x+fascia_punti){ //fasce verticali
                    if(kk >= p1.y  && kk<= p4.y ){
                        cv::Point2i p = cv::Point2i(jj,kk);
                        Vector_of_marker[0].AP_LM.push_back(p);
                        //cv::circle(color_mat,p,1,cv::Scalar(0,100,0));

                    }
                }

            }*/
        }

    }
    /*
    for(int kk = 0; kk < color_mat.rows ;kk++){
        for(int jj = 0; jj < color_mat.cols; jj++){
            if(p1.x < p4.x){
                if(jj >= p3.x-fascia_punti && jj <= p2.x+fascia_punti){ //fasce verticali
                    if(kk >= p2.y   && kk<= p3.y ){
                        cv::Point2i p = cv::Point2i(jj,kk);
                        Vector_of_marker[0].AP_RM.push_back(p);
                        // cv::circle(color_mat,p,1,cv::Scalar(0,0,200));

                    }
                }




            }
            else {
                if(jj >= p2.x -fascia_punti && jj <= p3.x+fascia_punti){ //fasce verticali
                    if(kk >= p2.y   && kk<= p3.y ){
                        cv::Point2i p = cv::Point2i(jj,kk);
                        Vector_of_marker[0].AP_RM.push_back(p);
                        //  cv::circle(color_mat,p,1,cv::Scalar(0,0,200));

                    }
                }
            }
        }
    }
    */


    //std::cout<<"size of the margin fascia left e right"<<Vector_of_marker[0].AP_LM.size()<<"   "<<Vector_of_marker[0].AP_RM.size()<<std::endl;



    // cv::imshow("punti nuovola aruco",color_mat);
    // cv::waitKey(1);




    return Vector_of_marker;

}

//FUNZIONE PER ESTRARRE PUNTI DAL MARKER E INFINE FARE LA MEDIA TRA I LORO VALORI DI PROFONDITA

std::vector<CAMERA_IMPLEMENTATION::ARUCO> CAMERA_IMPLEMENTATION::ARUCO_depth_extraction(cv::Mat image_of_depth , std::vector<ARUCO> Vector_of_marker, cv::Mat DP_mat){


    if (aruco_on_frame == true){
        if(Vector_of_marker.size() != 0 ){


            Vector_of_marker[0].id =  std::to_string(markerIds[0]);




            //estrazione corner 4


            if(Vector_of_marker[0].left_margin.size() != 0  && Vector_of_marker[0].right_margin.size() != 0){

                if(Vector_of_marker[0].vertici_aruco_depth.size() != 0 ){


                    Vector_of_marker[0].vertici_aruco_depth.pop_back();
                    Vector_of_marker[0].vertici_aruco_depth.pop_back();

                }



                cv::Point2i P1 =  cv::Point2i((int)Vector_of_marker[0].left_margin[0].x, (int)Vector_of_marker[0].left_margin[0].y);
                cv::Point2i P2 =  cv::Point2i((int)Vector_of_marker[0].right_margin[0].x, (int) Vector_of_marker[0].right_margin[0].y);


                cv::Point3d P1_vertice =  cv::Point3d(DP_mat.at<cv::Point3d>(P1.y,P1.x));
                cv::Point3d P2_vertice = cv::Point3d(DP_mat.at<cv::Point3d>(P2.y,P2.x));
                Vector_of_marker[0].vertici_aruco_depth.push_back(P1_vertice);
                Vector_of_marker[0].vertici_aruco_depth.push_back(P2_vertice);

                //   std::cout<<"vertice sinistro oprofondita  " << Vector_of_marker[0].vertici_aruco_depth[0]  << std::endl;



                Vector_of_marker[0].lato_alto_marker = sqrt(pow(P1_vertice.x-P2_vertice.x,2) + pow(P1_vertice.z-P2_vertice.z,2) +pow(P1_vertice.z-P2_vertice.z,2)  )  ;
                Vector_of_marker[0].lato_misure.push_back(Vector_of_marker[0].lato_alto_marker);
                double scartomedio = scarto_medio(Vector_of_marker[0].lato_misure);
                double Var = variance(Vector_of_marker[0].lato_misure);
                double mediaAr = Dmedia(Vector_of_marker[0].lato_misure);
                Vector_of_marker[0].media_lunghezza = mediaAr;

                //std::cout<<"media: " <<mediaAr <<"   scarto medio: " <<scartomedio <<"  varianza: " <<Var << std::endl;


                // std::cout<<"lato media aruco " << Vector_of_marker[0].media_lunghezza << std::endl;



            }






            cv::Point2i Punto_i_l;
            cv::Point3d punto_i_3D;
            cv::Point2i Punto_i_r;
            cv::Point3d punto_i_3DR;
            cv::Point3d sum_L = cv::Point3d(0,0,0);
            cv::Point3d sum_R = cv::Point3d(0,0,0);
            cv::Point3d media_L;
            cv::Point3d media_R;
            std::vector<std::vector<double>> Points_depth_allL(3);
            std::vector<std::vector<double>> Points_depth_allR(3);

            cv::Point2i Punto_i;
            cv::Point3d punto_3D;





            int left_i = 0;
            int right_i = 0;


            while ( Vector_of_marker[0].ALL_POINTS_MARKER_3D.size() != 0 ){

                Vector_of_marker[0].ALL_POINTS_MARKER_3D.pop_back();
            }



            for(int i = 0;i < Vector_of_marker[0].ALL_POINTS_MARKER.size();i++){
                Punto_i = Vector_of_marker[0].ALL_POINTS_MARKER[i];


                punto_3D = DP_mat.at<cv::Point3d>(Punto_i.y,Punto_i.x);
                cv::Point2d punto_2d_profondo =  cv::Point2d(punto_3D.x * zoom, punto_3D.z * zoom );
                Vector_of_marker[0].ALL_POINTS_MARKER_3D.push_back(punto_2d_profondo);







                cv::circle(image_of_depth,Punto_i,1,cv::Scalar(0,200,200));

            }





            //std::cout<<"size of APRM"<< Vector_of_marker[0].AP_RM.size()<<std::endl;
            for(int i = 0;i < Vector_of_marker[0].AP_LM.size();i++){
                Punto_i_l = Vector_of_marker[0].AP_LM[i];


                punto_i_3D = DP_mat.at<cv::Point3d>(Punto_i_l.y,Punto_i_l.x);
                Points_depth_allL[0].push_back(punto_i_3D.x*zoom);
                Points_depth_allL[1].push_back(punto_i_3D.y*zoom);
                Points_depth_allL[2].push_back(punto_i_3D.z*zoom);
                // std::cout<<"punto i 3d"<<punto_i_3D<<std::endl;


            }


            double mediaX = Dmedia(Points_depth_allL[0]);
            double mediaZ = Dmedia(Points_depth_allL[2]);

            double scartoX = scarto_medio(Points_depth_allL[0]);
            double scartoZ = scarto_medio(Points_depth_allL[2]);

            double Var__X = variance(Points_depth_allL[0]);
            double Var__Z = variance(Points_depth_allL[2]);




            media_L = cv::Point3d(media_mobile(Points_depth_allL[0],200),media_mobile(Points_depth_allL[1],200),media_mobile(Points_depth_allL[2],200));



            //std::cout<<"media x : "<<mediaX<<"  scarto medio x: "<<scartoX<<"media z : "<<mediaZ<<"  scarto medio z: "<<scartoZ<<std::endl;

            // std::cout<<"  scarto medio x: "<<scartoX<<"  scarto medio z: "<<scartoZ<<std::endl;



            Vector_of_marker[0].left_depth_margin = media_L;
            //std::cout<<"media calcolata per l e r gia in depth"<<media_L<<"   "<<media_R<<std::endl;

            for(int n = 0;n < Vector_of_marker[0].AP_RM.size();n++){
                Punto_i_r = Vector_of_marker[0].AP_RM[n];


                punto_i_3DR = DP_mat.at<cv::Point3d>(Punto_i_r.y,Punto_i_r.x);
                Points_depth_allR[0].push_back(punto_i_3DR.x*zoom);
                Points_depth_allR[1].push_back(punto_i_3DR.y*zoom);
                Points_depth_allR[2].push_back(punto_i_3DR.z*zoom);





            }
            double mediaXR = Dmedia(Points_depth_allL[0]);
            double mediaZR = Dmedia(Points_depth_allL[2]);

            double scartoXR = scarto_medio(Points_depth_allL[0]);
            double scartoZR = scarto_medio(Points_depth_allL[2]);

            double Var__XR = variance(Points_depth_allL[0]);
            double Var__ZR = variance(Points_depth_allL[2]);


            //inuile eliminare varianze superiori perche la media non cambia
            //std::cout<<"size before variance control"<< Points_depth_allL[0].size()<<std::endl;
            for(int j = 0; j < Points_depth_allR[0].size(); j ++){
                if( scarto_quadratico(Points_depth_allR[2][j], mediaZR) > 3*Var__ZR ){



                    Points_depth_allR[0].erase(Points_depth_allR[0].begin() + j);
                    Points_depth_allR[1].erase(Points_depth_allR[1].begin() + j);
                    Points_depth_allR[2].erase(Points_depth_allR[2].begin() + j);


                }
            }










            media_R = cv::Point3d(Dmedia(Points_depth_allR[0]),Dmedia(Points_depth_allR[1]),Dmedia(Points_depth_allR[2]));

            //std::cout<<"punti destri media   "<<media_R<<std::endl;


            Vector_of_marker[0].right_depth_margin = media_R;
            //std::cout<<"media calcolata per l e r gia in depth"<<media_L<<"   "<<media_R<<std::endl;

        }
    }

    return Vector_of_marker;

}

//FUNZIONE PER AGGIORNARE LA POSIZIONE DEL MARKER NEL FRAME

std::vector<CAMERA_IMPLEMENTATION::ARUCO> CAMERA_IMPLEMENTATION::ARUCO_upload(std::vector<ARUCO> Vector_of_marker){


    //pulisco il vettore punti 2D dai dati precedenti
    int ini_size_margin_l = Vector_of_marker[0].left_margin.size();
    int ini_size_margin_r = Vector_of_marker[0].right_margin.size();

    if ( Vector_of_marker[0].left_margin.size() != 0 &&  Vector_of_marker[0].right_margin.size() != 0){
        for (int ii = 0;ii < ini_size_margin_l;ii++){
            Vector_of_marker[0].left_margin.pop_back();
        }
        for (int in = 0;in < ini_size_margin_r;in++){
            Vector_of_marker[0].right_margin.pop_back();
        }
    }









    if (Vector_of_marker.size() != 0){
        if (markerCorners.size() != 0){

            Vector_of_marker[0].id =  std::to_string(markerIds[0]);




            cv::Point2i a = markerCorners[0][0];
            Vector_of_marker[0].left_margin.push_back(a);

            cv::Point2i b = markerCorners[0][3];
            Vector_of_marker[0].left_margin.push_back(b);



            cv::Point2i d = markerCorners[0][1];
            Vector_of_marker[0].right_margin.push_back(d);

            cv::Point2i e = markerCorners[0][2];
            Vector_of_marker[0].right_margin.push_back(e);
            Vector_of_marker[0].stereo_pose_extimation = pose_aruco_estimation;
            Vector_of_marker[0].stereo_asset_extimation = angolo_stima_aruco;





            return Vector_of_marker;




        }
    }



}









double CAMERA_IMPLEMENTATION::variance(std::vector<double> observations){

    observations = moving_avarege_lenght_control(observations);

    double media = Dmedia(observations);
    int element = 0;

    double somma_scarti_quadratici = 0;
    for(int i = 0; i < observations.size(); i ++){
        if(observations[i] != 0){

            double scarto_quadratico = pow(observations[i]-media,2);

            somma_scarti_quadratici += scarto_quadratico;
            element++;
        }
        else {
            observations.erase(observations.begin() + i);
        }


    }
    double scarto_quadatico_medio = somma_scarti_quadratici/element;
    return scarto_quadatico_medio;






}

double CAMERA_IMPLEMENTATION::Dmedia(std::vector<double> observations){

    observations = moving_avarege_lenght_control(observations);

    int element = 0;
    double sum = 0;

    for(int i = 0; i < observations.size(); i ++){
        if(observations[i] != 0){
            sum += observations[i];

            element++;



        }
        else {
            observations.erase(observations.begin() + i);
        }

    }
    double media = sum/element;
    return media;




}



double CAMERA_IMPLEMENTATION::media_mobile(std::vector<double> observations, int n_width){

    observations = moving_avarege_lenght_control(observations);



    int element = 0;
    double sum = 0;

    for(int i = 0; i < observations.size(); i ++){
        if(observations[i] != 0){
            sum += observations[i];

            element++;



        }
        else {
            observations.erase(observations.begin() + i);
        }

    }
    double media = sum/element;
    return media;


}




//}


double CAMERA_IMPLEMENTATION::scarto_medio(std::vector<double> observations){

    observations = moving_avarege_lenght_control(observations);
    double media = Dmedia(observations);
    int element = 0;


    double somma_scarti = 0;
    for(int i = 0; i < observations.size(); i ++){
        if(observations[i] != 0){

            double scarto = sqrt(pow(observations[i]-media,2));

            somma_scarti += scarto;
            element++;
        }
        else {
            observations.erase(observations.begin() + i);
        }


    }
    double scarto_medio = somma_scarti/element;
    return scarto_medio;





}




double CAMERA_IMPLEMENTATION::scarto_quadratico(double valore, double media){

    double Var =  pow(valore-media,2);
    return Var;

}



/*

void CAMERA_IMPLEMENTATION::calibrate_camera_aruco(cv::Mat camera_matrix, cv::Mat dist_coeff){
    cv::Ptr<aruco::CharucoBoard> board =
     cv::Size imgSize =
    std::vector<std::vector<cv::Point2f>> allCharucoCorners;
    std::vector<std::vector<int>> allCharucoIds;
    cv::Mat cameraMatrix, distCoeffs;
    std::vector<cv::Mat> rvecs, tvecs;
    double repError = cv::aruco::calibrateCameraCharuco(allCharucoCorners, allCharucoIds, board, imgSize, cameraMatrix, distCoeffs, rvecs, tvecs, calibrationFlags);
    
    
    
}


*/


std::vector<CAMERA_IMPLEMENTATION::ARUCO> CAMERA_IMPLEMENTATION::extract_mask_point(cv::Mat input_image, std::vector<ARUCO> MARKERS){

    cv::Mat B_image_dest(input_image.rows,input_image.cols,CV_8UC1);

    std::vector<cv::Point2i> corners;
    corners.push_back(MARKERS[0].left_margin[0]);
    corners.push_back(MARKERS[0].left_margin[1]);
    corners.push_back(MARKERS[0].right_margin[0]);
    corners.push_back(MARKERS[0].right_margin[1]);

    cv::drawContours(B_image_dest,corners,0,cv::Scalar(255,255,255), CV_FILLED);
    cv::imshow("contours", B_image_dest);
    cv::waitKey(1);
    return MARKERS;



}

std::vector<double> CAMERA_IMPLEMENTATION::moving_avarege_lenght_control(std::vector<double> observations){
    int max_length = 300;
    while (observations.size() > max_length ){
        observations.erase(observations.begin());

    }
    return observations;

}



// Checks if a matrix is a valid rotation matrix.
bool CAMERA_IMPLEMENTATION::isRotationMatrix(cv::Mat &R)
{
    cv::Mat Rt;
    transpose(R, Rt);
    cv::Mat shouldBeIdentity = Rt * R;
    cv::Mat I = cv::Mat::eye(3,3, shouldBeIdentity.type());

    return  norm(I, shouldBeIdentity) < 1e-6;

}





// Calculates rotation matrix to euler angles
// The result is the same as MATLAB except the order
// of the euler angles ( x and z are swapped ).



cv::Vec3f CAMERA_IMPLEMENTATION::rotationMatrixToEulerAngles(cv::Mat &R)
{

    assert(isRotationMatrix(R));

    float sy = sqrt(R.at<double>(0,0) * R.at<double>(0,0) +  R.at<double>(1,0) * R.at<double>(1,0) );

    bool singular = sy < 1e-6; // If

    float x, y, z;
    if (!singular)
    {
        x = atan2(R.at<double>(2,1) , R.at<double>(2,2));
        y = atan2(-R.at<double>(2,0), sy);
        z = atan2(R.at<double>(1,0), R.at<double>(0,0));
    }
    else
    {
        x = atan2(-R.at<double>(1,2), R.at<double>(1,1));
        y = atan2(-R.at<double>(2,0), sy);
        z = 0;
    }
    return cv::Vec3f(x, y, z);



}






