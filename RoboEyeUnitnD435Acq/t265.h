#ifndef T265_H
#define T265_H
#include <librealsense2/rs.hpp>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <math.h>
#include <thread>
#include <QtCore>
#include <string>
#include <fstream>


#define PI 3.14159265
class T265 : public QThread
{
public:
    T265();
    struct Quaternion {
        double w, x, y, z;
    };

    struct EulerAngles {
        double roll, pitch, yaw;
    };
    cv::Point3d left_depth_aruco;
    cv::Point3d right_depth_aruco;

    EulerAngles Quat_2_euler(Quaternion q);

    void GenerateMap();
    void InizializeT265();
    void TestSensor();
    void run();
    void Draw_generate_wall_aruco(cv::Mat fixed_image, cv::Mat map2draw,cv::Point2d left_aruco,cv::Point2d right_aruco, cv::Scalar color);
    cv::Point2d Trasform_D435LocalP_2_WORLD(double theta, int Pt_0_x,int Pt_0_y, double PL_x,double PL_y );
    double distanza_2_punti2D (cv::Point2d P1, cv::Point2d P2);
    std::vector<std::vector<double>> fit_line(cv::Mat image,std::vector<std::vector<double>> XYTHETA_final_depth, std::vector<cv::Point2d> points, std::vector<double> vel_omega, std::vector<double> positions_from_aruco_vector_x, std::vector<double> positions_from_aruco_vector_y, std::vector<double> angles);
    void ellipse_static_robot(cv::Scalar color,cv::Mat image ,  std::vector<double> positions_from_aruco_vector_x, std::vector<double> positions_from_aruco_vector_y , std::vector<double> angles);
    void fusion_dist_percorsa(cv::Mat image , std::vector<double> x_aruco,std::vector<double> y_aruco, std::vector<double> theta_aruco, std::vector<double> x_t265,std::vector<double> y_t265,  std::vector<double> theta_t265);
    cv::Mat covariance_matrix_2(std::vector<double> observation_x,std::vector<double> observation_y );
    double covariance (std::vector<double> observation_x,std::vector<double> observation_y );
    std::vector<cv::Point2d> sensor_fusion_static_D435(cv::Mat image , std::vector<double> observation_x,std::vector<double> observation_y , std::vector<double> observation_zx,std::vector<double> observation_zy);
    cv::Mat covariance_matrix_3(std::vector<double> observation_x,std::vector<double> observation_y , std::vector<double> observation_z);
    cv::Vec3d sensor_fusion_static_D435_3(cv::Mat image , std::vector<double> observation_x,std::vector<double> observation_y ,std::vector<double> observation_z, std::vector<double> observation_zx,std::vector<double> observation_zy,std::vector<double> observation_zz);
    std::vector<std::vector<double>> trajectory_global_implementation(cv::Mat image , cv::Vec3d last_fused_position, std::vector<std::vector<double>> T265_info, std::vector<std::vector<double>> T265_global);

    std::vector<std::vector<double>> manage_XYtheta_from_stereo_aruco(cv::Mat image_fixed , std::vector<std::vector<double>> RGB_POSE_TRANS , cv::Mat image ,cv::Point2d pose, double angle,std::vector<double> X_pose, std::vector<double> Y_pose, std::vector<double> asset_theta, std::vector<std::vector<double>> XYtheta_vector_stereo);
    //______________
    cv::Vec3d fusion_T265_D435_trj (std::vector<std::vector<double>> T265_trj_transf, std::vector<std::vector<double>> D435_global_withT265,cv::Vec3d startP,cv::Mat image, cv::Mat fixed_image,std::vector<std::vector<double>> raw_D435_pose_data, std::vector<std::vector<double>> velocity_current_data,  std::vector<std::vector<double>> T265_odometry_pose,std::vector<std::vector<double>> fused_trj_final,std::vector<std::vector<double>> D435_local_data);
    cv::Vec3d uncertanty_dinamic_generator_D435(double vx,double vy, double vtheta);
    std::vector<std::vector<double>> T265_trj_GLOBAL;
    std::vector<std::vector<double>> D435_global_angleT265;
    std::vector<std::vector<double>> fused_TRJ_DT;
    cv::Vec3d Start_point;
    double sigma_theta;

    //________________
    std::vector<std::vector<double>> XYtheta_stereo_aruco;
    std::vector<cv::Point2d> punti_statici_fusi;
    std::vector<std::vector<double>> info_XYtheta_from_depth;
    std::vector<std::vector<double>> T265_local_xytheta;
    std::vector<std::vector<double>>  T265_transformed_global;
    std::vector<std::vector<double>> RGB_pose_transformed;
    std::vector<std::vector<double>> T265_velocity_xyomega;

    double variance(std::vector<double> observations);
    std::vector<double> n_punti_marker ;


    double Dmedia(std::vector<double> observations);

    double scarto_medio(std::vector<double> observations);
    double media_mobile(std::vector<double> observations, int n_width);
    std::vector<double> moving_avarege_lenght_control(std::vector<double> observations);
    void write_vector_csv_file(std::vector<double> vector, std::string nome);
    void write_vector_csv_file_int(std::vector<int> vector, std::string nome );


    std::vector<double> angoli_depth;
    cv::Mat image_trj;

    int map_dim = 2000;
    std::vector<cv::Point2i> trj;//impongo un limite di punti
    cv::Point pt;
    int thickness = 2;
    cv::Point pax;
    cv::Point pay;
    cv::Vec3d punto_base;
    double angle;
    const int l = 50;
    const int zoom = 100;
    int i = 0;
    bool pointbreak;
    std::vector<cv::Point2i> pointcloud;
    cv::Point3d markerPointL;
    cv::Point3d markerPointR;
    cv::Point3d markerPointR_l;
    cv::Point3d markerPointL_l;
    cv::Point2d Marker2DMapL;

    cv::Point2d Marker2DMapR;
    std::vector<cv::Point2d> all_points_marker_3d;
    cv::Point2d LW;
    cv::Point2d RW;
    bool inizio_differenziale;


    rs2_pose pose;
    rs2_vector accel_sample;
    rs2_vector gyro_sample;
    rs2::pipeline pipe;
    rs2::config cfg;
    rs2::frameset frames;
    rs2::frameset* framesptr = &frames;
    rs2::pipeline_profile pipeline;
    rs2::context ctx;
    rs2::device dev;
    rs2::pipeline_profile profile;



    std::vector<double> x_pos_aruco_robot;
    std::vector<double> y_pos_aruco_robot;
    std::vector<double> x_pos_t265_robot;
    std::vector<double> y_pos_t265_robot;
    std::vector<double> theta_aruco_robot;
    std::vector<double> theta__t265_robot;

    double stereo_angle;
    cv::Point2d stereo_pose;
    std::vector<double> stereo_POSES_x;
    std::vector<double> stereo_POSES_y;
    std::vector<double> stereo_ANGLES;
    std::vector<std::vector<double>> fused_relative_XYtheta;

    int jj;


private:
    std::vector<cv::Point2i> TRJ;
    cv::Mat imageToDraw;

    cv::Point2d left_aruco_trasformed_global;
    cv::Point2d right_aruco_trasformed_global;
    bool marker_muro_fisso;
    std::vector<double> angoli_rotazione;
    std::vector< std::vector<double> > punti_muro_23;
    std::vector<double> muro_aruco_L_x;
    std::vector<double> muro_aruco_L_y;
    std::vector<double> muro_aruco_R_x;

    std::vector<double> muro_aruco_R_y;







};

#endif // T265_H
