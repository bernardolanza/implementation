#include "t265.h"

#include <QtCore>
#include <QDebug>

T265::T265()
{

}
void T265::run()
{

    qDebug() << "runningT265";

    GenerateMap();







}


void T265::InizializeT265(){
    //INIZIALIZZAZION T265

    // rs2::device_list devices = ctx.query_devices(); // Get a snapshot of currently connected devices
    /*
    if (devices.size() == 0)	{
        std::cout<< "No device detected. Is it plugged in?"<<std::endl;
    }
    else if(devices.size() == 1){
        std::cout<< "Solo un dispositivo rilevato, non sono programmato per questo"<<std::endl;

    }
    else {
        std::cout << "found " << devices.size() << " realsense devices." << std::endl;
        for (size_t i = 0; i < devices.size(); i++) {
           rs2::device dev = devices[i];
           std::string devSerialNumber = dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
           std::cout << "dev " << i << " -> SN= " << devSerialNumber << std::endl;
}

}
*/

    std::string devSerialNumberT265  = "852212110836";
    // rs2::pipeline pipe(ctx);
    //   cfg.enable_device(devSerialNumberT265);
    // std::cout << "T265 enabled" <<  std::endl;
    cfg.enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF);// Add pose stream
    cfg.enable_stream(RS2_STREAM_GYRO);
    cfg.enable_stream(RS2_STREAM_ACCEL);



    imageToDraw = cv::Mat(map_dim,map_dim, CV_8UC3, cv::Scalar(0, 0, 0));



    //std::this_thread::sleep_for (std::chrono::seconds(10));

    std::cout <<"sensors T265 inizialized succesfully"<<std::endl;


    marker_muro_fisso = false;

    left_depth_aruco = cv::Point3d(0,0,0);
    right_depth_aruco = cv::Point3d(0,0,0);

    x_pos_aruco_robot.reserve(300);
    y_pos_aruco_robot.reserve(300);




    std::vector<double> a;
    std::vector<double> b;
    std::vector<double> c;
    info_XYtheta_from_depth.push_back(a);
    info_XYtheta_from_depth.push_back(b);
    info_XYtheta_from_depth.push_back(c);
    fused_relative_XYtheta.push_back(a);
    fused_relative_XYtheta.push_back(b);
    fused_relative_XYtheta.push_back(c);
    T265_local_xytheta.push_back(a);
    T265_local_xytheta.push_back(b);
    T265_local_xytheta.push_back(c);
    RGB_pose_transformed.push_back(a);
    RGB_pose_transformed.push_back(a);
    RGB_pose_transformed.push_back(a);
    T265_transformed_global.push_back(a);
    T265_transformed_global.push_back(a);
    T265_transformed_global.push_back(a);
    T265_velocity_xyomega.push_back(a);
    T265_velocity_xyomega.push_back(a);
    T265_velocity_xyomega.push_back(a);
    XYtheta_stereo_aruco.push_back(a);
    XYtheta_stereo_aruco.push_back(b);
    XYtheta_stereo_aruco.push_back(c);
    fused_TRJ_DT.push_back(a);
    fused_TRJ_DT.push_back(a);
    fused_TRJ_DT.push_back(a);
    T265_trj_GLOBAL.push_back(a);
    T265_trj_GLOBAL.push_back(a);
    T265_trj_GLOBAL.push_back(a);
    D435_global_angleT265.push_back(a);
    D435_global_angleT265.push_back(a);
    D435_global_angleT265.push_back(a);


    Start_point[0] = 0;
    Start_point[1] = 0;
    Start_point[2] = 0;

    image_trj = cv::Mat(map_dim,map_dim, CV_8UC3, cv::Scalar(0, 0, 0));
    inizio_differenziale = false;



    jj = 0;






    pipe.start(cfg);



    //GenerateMap();

}





void T265::GenerateMap(){


    cv::Mat aximage;
    cv::Mat pointcloudshot;

    // std::cout <<"pointcloud.size ..." <<pointcloud.size()<<std::endl;
    std::vector<cv::Point2i> pointcloud_global;



    // cv::imshow("glob_mat", globalMap);
    //  cv::waitKey(1);



    //std::cout<<"steroe pose x "<<stereo_pose.x<<std::endl;


    //MAPPA SDR WORLD


    //cv::Mat img2Show;
    //cv::cvtColor(colorMat, img2Show, cv::COLOR_BGR2RGB);
    //cv::resize(imageToDraw, imageToDraw, cv::Size(), 2, 2);



    //mutex.lock();


    //  std::cout<<"before while loop"<<std::endl;

    //mutex.unlock();




    // Wait for the next set of frames from the camera
    frames = pipe.wait_for_frames();
    // Get a frame from the pose stream

    rs2::pose_frame frameAlphaPose = frames.first_or_default(RS2_STREAM_POSE);

    if (rs2::motion_frame accel_frame = frames.first_or_default(RS2_STREAM_ACCEL))
    {
        accel_sample = accel_frame.get_motion_data();
        // std::cout << "Accel:" << accel_sample.x << ", " << accel_sample.y << ", " << accel_sample.z << std::endl;

    }

    if (rs2::motion_frame gyro_frame = frames.first_or_default(RS2_STREAM_GYRO))
    {
        gyro_sample = gyro_frame.get_motion_data();
        //std::cout << "Gyro:" << gyro_sample.x << ", " << gyro_sample.y << ", " << gyro_sample.z << std::endl;

    }
    pose = frameAlphaPose.as<rs2::pose_frame>().get_pose_data();


    // std::cout << "translation x , y , z : " << pose.rotation.y <<  std::endl ;
    //usleep(1000);



    //TRAIETTORIA


    // pt.x = (pose.translation.x*zoom)+map_dim/2;

    // pt.y = (pose.translation.z*zoom)+map_dim/2;


    //TRAIETTORIA XY

    pt.x = pose.translation.x*zoom+map_dim/2;
    pt.y = pose.translation.z*zoom+map_dim/2;

    x_pos_t265_robot.push_back(pose.translation.x*zoom);
    y_pos_t265_robot.push_back(pose.translation.z*zoom);
    x_pos_t265_robot = moving_avarege_lenght_control(x_pos_t265_robot);
    y_pos_t265_robot = moving_avarege_lenght_control(y_pos_t265_robot);




    double vx = pose.velocity.x;
    double vz = pose.velocity.z;
    double omegay = pose.angular_velocity.y;
    std::vector<double> vel_xy_omega;
    vel_xy_omega.push_back(vx);
    vel_xy_omega.push_back(vz);
    vel_xy_omega.push_back(omegay);
    //:_______________________________________________________VELOCITY TRIPLE VECTOR T265
    T265_velocity_xyomega[0].push_back(vx);
    T265_velocity_xyomega[1].push_back(vz);
    T265_velocity_xyomega[2].push_back(omegay);

    T265_velocity_xyomega[0] = moving_avarege_lenght_control(T265_velocity_xyomega[0]);
    T265_velocity_xyomega[1] = moving_avarege_lenght_control(T265_velocity_xyomega[1]);
    T265_velocity_xyomega[2] = moving_avarege_lenght_control(T265_velocity_xyomega[2]);

    //std::cout<< "pose value"<< stereo_pose <<std::endl;




    //QUATERNIONI DATI DALLA ROTAZIONE DEL GIROSCOPIO
    Quaternion q;


    q.w = pose.rotation.w;
    q.x = pose.rotation.x;
    q.y = pose.rotation.y;
    q.z = pose.rotation.z;

    EulerAngles EUANG =  Quat_2_euler(q);

    angle = EUANG.pitch; //CONVERSIONE IN SISTEMA EULERIANO



    // theta__t265_robot.push_back(angle);
    //theta__t265_robot = moving_avarege_lenght_control(theta__t265_robot);

    //angoli_rotazione.push_back(angle);
    //______________________________________________________________________________________T265 data pose triple vector
    T265_local_xytheta[0].push_back(pose.translation.x*zoom);
    T265_local_xytheta[1].push_back(-pose.translation.z*zoom);
    T265_local_xytheta[2].push_back(angle);

    T265_local_xytheta[0] = moving_avarege_lenght_control(T265_local_xytheta[0]);
    T265_local_xytheta[1] = moving_avarege_lenght_control(T265_local_xytheta[1]);
    T265_local_xytheta[2] = moving_avarege_lenght_control(T265_local_xytheta[2]);




    //PUNTI TRAIETTORIA ROBOT
    trj.push_back(pt);


    //std::cout <<"angle : "<< angle << std::endl;
    //ASSI SISTEMA DI RIFERIMENTO SOLIDAL AL ROBOT
    pax.x = pt.x+l*cos(angle); //(blu)
    pax.y = pt.y-l*sin(angle);
    pay.x = pt.x+l*cos(angle+PI/2);
    pay.y = pt.y-l*sin(angle+PI/2); //meno perche per andare in alto sulla mappa bisogna diminuire la y dell aMAt
    //std::cout << "angle rotation :  "<< angle<<std::endl;
    //  std::cout <<pt.x<<"  "<<pt.y<<std::endl;
    // std::cout <<"pitch : "<< EUANG.pitch<<"  roll : "<< EUANG.roll<<"  yaw : "<< EUANG.yaw <<std::endl;


    //CREAZIONE MAPPA

    if(i>1){
        cv::Mat image = imageToDraw.clone();


        //GRAFICO TRAIETTORIA
        //   cv::line(imageToDraw,trj[i-1],trj[i],cv::Scalar( 0, 0, 255 ),thickness);//trajectories
        imageToDraw.copyTo(aximage);


        cv::line(aximage,trj[i],pax,cv::Scalar( 0, 255, 0),thickness);
        cv::line(aximage,trj[i],pay,cv::Scalar( 255, 0, 0),thickness);


        //reinizzializzo il pointcloud che fotografa i muri con lA mappa senza muri ma con la trajettoria aggiornata
        aximage.copyTo(pointcloudshot);
        //IMPORTO IL POINTCLOUD DELLA D435 A FASCIA
        // std::cout<<"POINT world : : "<<pointcloud.size()<<std::endl;




        for(int v = 0; v < pointcloud.size(); v++){


            cv::Point2i PT_world_GL;
            PT_world_GL = Trasform_D435LocalP_2_WORLD(angle,pt.x,pt.y,pointcloud[v].x,pointcloud[v].y);
            //FUNZIONE PER FARE UNA TRAASFORMAZIONE TRA SISTEMI DI RIF-glERIMENTO LOCALE(D435) E GLOBALE (T265)




            pointcloud_global.push_back(PT_world_GL);
            //GRAFICO I POINTCLOUD TRASFORMATI
            cv::circle(pointcloudshot,PT_world_GL,1,cv::Scalar(0,255,0));
            //std::cout<<"POINT world : : "<<PT_world_GL<<std::endl<<std::endl;







        }






        if(left_depth_aruco != cv::Point3d(0,0,0) && right_depth_aruco != cv::Point3d(0,0,0)){




            //TRASFORMO ANCHE I PUNTI DEL MARKER

            left_aruco_trasformed_global = Trasform_D435LocalP_2_WORLD(angle,pt.x,pt.y,left_depth_aruco.x,left_depth_aruco.z);



            right_aruco_trasformed_global = Trasform_D435LocalP_2_WORLD(angle,pt.x,pt.y,right_depth_aruco.x,right_depth_aruco.z);



            // cv::circle(pointcloudshot,right_aruco_trasformed_global,5,cv::Scalar(255,255,0));
            //   cv::circle(pointcloudshot,left_aruco_trasformed_global,5,cv::Scalar(255,255,0));





            // cv::circle(pointcloudshot,right_aruco_trasformed_global[0],20,cv::Scalar(255,255,0));
            //cv::circle(pointcloudshot,left_aruco_trasformed_global[0],20,cv::Scalar(255,255,0));





        }

        else {
            left_aruco_trasformed_global = cv::Point2d(0,0);
            right_aruco_trasformed_global = cv::Point2d(0,0);
        }





        //double distanza_muro = sqrt(pow(pt.x-left_aruco_trasformed_global.x,2)+pow(pt.y-left_aruco_trasformed_global.y,2));
        double distanza_muro = distanza_2_punti2D(pt,left_aruco_trasformed_global);
        // std::cout<<"distanza muro aruco left   "<< distanza_muro<<std::endl;

        //GENERO IL MURO ASSOCIATO AL MARKER NEI PUNTI DEL MARKER GLOBALI WRT WORLD
        if(left_aruco_trasformed_global != cv::Point2d(0,0) && right_aruco_trasformed_global != cv::Point2d(0,0)){


            Draw_generate_wall_aruco(imageToDraw,pointcloudshot,left_aruco_trasformed_global,right_aruco_trasformed_global, cv::Scalar(0,0,255));


            // std::cout<<"size vec"<<x_pos_aruco_robot.size()<<std::endl;

            ////____________________________________STATIC ANALISIS____________________________________///









            XYtheta_stereo_aruco[0].push_back(-stereo_pose.x);
            XYtheta_stereo_aruco[1].push_back(stereo_pose.y);
            XYtheta_stereo_aruco[2].push_back(stereo_angle);

            ////___________________________________________________________________________________from local to global T265

            RGB_pose_transformed = manage_XYtheta_from_stereo_aruco(image_trj,  RGB_pose_transformed,image , stereo_pose,stereo_angle,stereo_POSES_x,stereo_POSES_y,stereo_ANGLES,XYtheta_stereo_aruco);


            XYtheta_stereo_aruco[0] = moving_avarege_lenght_control( XYtheta_stereo_aruco[0]);
            XYtheta_stereo_aruco[1] = moving_avarege_lenght_control( XYtheta_stereo_aruco[1]);
            XYtheta_stereo_aruco[2] = moving_avarege_lenght_control( XYtheta_stereo_aruco[2]);
            RGB_pose_transformed[0] = moving_avarege_lenght_control( RGB_pose_transformed[0]);
            RGB_pose_transformed[1] = moving_avarege_lenght_control( RGB_pose_transformed[1]);
            RGB_pose_transformed[2] = moving_avarege_lenght_control( RGB_pose_transformed[2]);





            /*

            if( RGB_pose_transformed[0].size() > 10 ){
                ellipse_static_robot(cv::Scalar(0,255,0),image, RGB_pose_transformed[0],RGB_pose_transformed[1], RGB_pose_transformed[2]);
                cv::putText(image,"stereo rgb camera",cv::Point2i( map_dim - 200, map_dim-100),cv::FONT_HERSHEY_COMPLEX_SMALL,1, cv::Scalar(0,255,0));

            }


            if (vel_xy_omega[0] < 1){
                if (vel_xy_omega[1] < 1){
                    if (vel_xy_omega[2] < 1){
                        if( inizio_differenziale == false){

                            punto_base = cv::Vec3d(RGB_pose_transformed[0].back(), RGB_pose_transformed[1].back(),RGB_pose_transformed[2].back());


                            inizio_differenziale = true;




                        }
                    }
                }
            }
            // std::cout<<"punto base :  "<<punto_base<<std::endl;



*/













            info_XYtheta_from_depth = fit_line(image, info_XYtheta_from_depth,all_points_marker_3d, vel_xy_omega,x_pos_aruco_robot,y_pos_aruco_robot,angoli_depth);



            info_XYtheta_from_depth[0] = moving_avarege_lenght_control( info_XYtheta_from_depth[0]);
            info_XYtheta_from_depth[1] = moving_avarege_lenght_control( info_XYtheta_from_depth[1]);
            info_XYtheta_from_depth[2] = moving_avarege_lenght_control( info_XYtheta_from_depth[2]); //non funzionante, va cambiato il return di fitline








            //  if( info_XYtheta_from_depth[0].size() != 0 &&  XYtheta_stereo_aruco[0].size() != 0 ){


            //cv::Vec3d XYtheta_fused = sensor_fusion_static_D435_3(image, info_XYtheta_from_depth[0], info_XYtheta_from_depth[1],info_XYtheta_from_depth[2],XYtheta_stereo_aruco[0],XYtheta_stereo_aruco[1],XYtheta_stereo_aruco[2]);
            // fused_relative_XYtheta[0].push_back(XYtheta_fused[0]);
            //  fused_relative_XYtheta[1].push_back(XYtheta_fused[1]);
            //   fused_relative_XYtheta[2].push_back(XYtheta_fused[2]);

            // fused_relative_XYtheta[0] = moving_avarege_lenght_control( fused_relative_XYtheta[0]);
            //  fused_relative_XYtheta[1] = moving_avarege_lenght_control( fused_relative_XYtheta[1]);
            //   fused_relative_XYtheta[2] = moving_avarege_lenght_control( fused_relative_XYtheta[2]);

            //  cv::Point2d punto_fuso_trasformato = Trasform_D435LocalP_2_WORLD(XYtheta_fused[2],0,0,XYtheta_fused[0],XYtheta_fused[1]);
            //  cv::circle(image,cv::Point2d(punto_fuso_trasformato.x +map_dim/2,punto_fuso_trasformato.y +map_dim/2) , 10,cv::Scalar(100,255,100),2);
            //  cv::line(image,cv::Point2d(punto_fuso_trasformato.x +map_dim/2,punto_fuso_trasformato.y +map_dim/2), cv::Point2d(punto_fuso_trasformato.x +map_dim/2-30*sin(XYtheta_fused[2]),punto_fuso_trasformato.y +map_dim/2 - 30*cos(XYtheta_fused[2])), cv::Scalar(0,0,255),2);
            // cv::circle(image_trj,cv::Point2d(punto_fuso_trasformato.x +map_dim/2,punto_fuso_trasformato.y +map_dim/2) , 1,cv::Scalar(100,255,100),2);


            //  n_punti_marker.push_back(all_points_marker_3d.size());


            double ptsize = all_points_marker_3d.size();
            //  std::cout<<"punti.... "<< ptsize<<std::endl;
            n_punti_marker.push_back(ptsize);
            n_punti_marker = moving_avarege_lenght_control( n_punti_marker);
            // std::cout<<" i  "<<i<<std::endl;

            //  cv::putText(image, std::to_string(variance(fused_relative_XYtheta[0])),cv::Point2i( map_dim - 200, map_dim-500),cv::FONT_HERSHEY_COMPLEX_SMALL,2, cv::Scalar(0,0,255));
            // cv::putText(image, std::to_string(variance(fused_relative_XYtheta[2])),cv::Point2i( map_dim - 200, map_dim-600),cv::FONT_HERSHEY_COMPLEX_SMALL,2, cv::Scalar(0,0,255));




            //  if(fused_relative_XYtheta[2].size()> 30){
            //      if ( variance(fused_relative_XYtheta[2]) < 0.05 ){
            //         if( inizio_differenziale == false){

            //             punto_base = XYtheta_fused;


            //             inizio_differenziale = true;

            //      }
            //   }

            //  }






            /*


                write_vector_csv_file(fused_relative_XYtheta[0], "x_fused");
                write_vector_csv_file(fused_relative_XYtheta[1], "y_fused");
                write_vector_csv_file(fused_relative_XYtheta[2], "theta_fused");



                write_vector_csv_file(T265_local_xytheta[0], "x_t265");
                write_vector_csv_file(T265_local_xytheta[1], "y_t265");
                write_vector_csv_file(T265_local_xytheta[2], "theta_t265");




                write_vector_csv_file(info_XYtheta_from_depth[0], "x_depth");
                write_vector_csv_file(info_XYtheta_from_depth[1], "y_depth");
                write_vector_csv_file(info_XYtheta_from_depth[2], "theta_depth");
                write_vector_csv_file(n_punti_marker, "puntimarker");

                write_vector_csv_file(XYtheta_stereo_aruco[0], "x_rgb");
                write_vector_csv_file(XYtheta_stereo_aruco[1], "y_rgb");
                write_vector_csv_file(XYtheta_stereo_aruco[2], "theta_rgb");*/




            //}
            /*
            write_vector_csv_file(T265_local_xytheta[0], "x_t265");
            write_vector_csv_file(T265_local_xytheta[1], "y_t265");
            write_vector_csv_file(T265_local_xytheta[2], "theta_t265");



            write_vector_csv_file(T265_velocity_xyomega[0], "vx_rgb");
            write_vector_csv_file(T265_velocity_xyomega[1], "vy_rgb");
            write_vector_csv_file(T265_velocity_xyomega[2], "vtheta_rgb");
            write_vector_csv_file(n_punti_marker, "puntimarker");




            write_vector_csv_file(RGB_pose_transformed[0], "x_rgb_trans");
            write_vector_csv_file(RGB_pose_transformed[1], "y_rgb_trans");
            write_vector_csv_file(RGB_pose_transformed[2], "theta_rgb_trans");


*/
            write_vector_csv_file(XYtheta_stereo_aruco[0], "x_rgb");
            write_vector_csv_file(XYtheta_stereo_aruco[1], "y_rgb");
            write_vector_csv_file(XYtheta_stereo_aruco[2], "theta_rgb");


            if(distanza_muro < 2500.0){ //threshold di vicinanza per disegnare il muro solo da vicin
                //implementa il filtro direzionale, disegna muro solo quando sei davanti non di lato?
                if (distanza_muro > 10 ){






                    muro_aruco_L_x.push_back(left_aruco_trasformed_global.x);
                    muro_aruco_L_y.push_back(left_aruco_trasformed_global.y);
                    muro_aruco_R_x.push_back(right_aruco_trasformed_global.x);
                    muro_aruco_R_y.push_back(right_aruco_trasformed_global.y);

                }
            }



            /*
            if(muro_aruco_L_x.size() > 10){
                LW = cv::Point2d(media_mobile(muro_aruco_L_x,200),media_mobile(muro_aruco_L_y, 200));
                RW = cv::Point2d(media_mobile(muro_aruco_R_x,200),media_mobile(muro_aruco_R_y, 200));
                double distanza_corner = distanza_2_punti2D(LW,RW);
                //  std::cout<<"distanza corner aruco   "<< distanza_corner<<std::endl;

                marker_muro_fisso = true;

            }
*/






            //double distanza_aruco = distanza_2_punti2D(LW,RW);
            // std::cout<<"distanza aruco t265   :"<<distanza_aruco <<std::endl;







            // Draw_generate_wall_aruco(imageToDraw,imageToDraw,LW,RW);

        }



        //____________________________________________________________________________________________________FUSION
        //___________________________________________________________________________________________________________________



        if(T265_velocity_xyomega[0].size() > 10){


            Start_point =  fusion_T265_D435_trj(T265_trj_GLOBAL,D435_global_angleT265,Start_point,image,imageToDraw,RGB_pose_transformed,T265_velocity_xyomega,T265_local_xytheta,fused_TRJ_DT,XYtheta_stereo_aruco);


        }









        //____________________________________________________________________________________________________FUSION



        if (false ) //bloccato
        {


            T265_transformed_global =  trajectory_global_implementation(image_trj,punto_base,T265_local_xytheta,T265_transformed_global);
            cv::putText(image,"differential TRAJ",cv::Point2i( map_dim - 300, map_dim-400),cv::FONT_HERSHEY_COMPLEX_SMALL,1, cv::Scalar(0,255,255));

            cv::putText(image, "x , y , theta local T265 " + std::to_string((int)T265_local_xytheta[0].back())+ " ; " +std::to_string((int)T265_local_xytheta[1].back())+ " ; " +std::to_string((int)(T265_local_xytheta[2].back()*180/PI)),cv::Point2i( map_dim - 600, map_dim-500),cv::FONT_HERSHEY_COMPLEX_SMALL,1, cv::Scalar(0,255,255));
            cv::putText(image, "x , y , theta relative RGB  " + std::to_string((int)XYtheta_stereo_aruco[0].back())+ " ; " +std::to_string((int)XYtheta_stereo_aruco[1].back())+ " ; " +std::to_string((int)(XYtheta_stereo_aruco[2].back()*180/PI)),cv::Point2i( map_dim - 600, map_dim-600),cv::FONT_HERSHEY_COMPLEX_SMALL,1, cv::Scalar(0,255,255));

        }

        cv::Mat imageV;
        cv::resize(image, imageV, cv::Size(), 0.5, 0.5);
        cv::Rect roi(900, 900, 400,400);
        cv::Mat zoomed = image(roi);

        // cv::imshow("teddmp", imageV);
        //  cv::imshow("tefffddmp", image_trj);

        // cv::waitKey(1);

        cv::resize(zoomed, zoomed, cv::Size(), 3, 3);
        cv::imshow("tedd77mp", zoomed);
        cv::waitKey(1);



        /*
        if(marker_muro_fisso == true){

            Draw_generate_wall_aruco(imageToDraw,pointcloudshot,LW,RW,cv::Scalar(0,255,0) ); // cambia colore
        }
*/
        // cv::imshow("Display eyes", pointcloudshot); //in visualizzazione
        //  cv::waitKey(1);


    }



    i++;



    //    }

    // }



    pointcloud.clear(); //svuoto pointcloud vector prima del nuovo arrivo







}




//FUNZIONE DI TEST T265
void T265::TestSensor(){

}

//FUNZIONE CHE TRASFORMA UN PUNTO LOCALE IN UNO GLOBALE

cv::Point2d T265::Trasform_D435LocalP_2_WORLD(double theta, int Pt_0_x,int Pt_0_y, double PL_x,double PL_y ){

    cv::Point2d PT_world;
    cv::Mat R_global = (cv::Mat_<double>(3,3) << cos(theta),+sin(theta), 0,-sin(angle),cos(theta),0,0,0,1);

    cv::Vec3d Pt_0 ;
    cv::Vec3d Pt_lg;
    Pt_0[0] = Pt_0_x;
    Pt_0[1] = Pt_0_y;
    Pt_0[2] = 0;

    cv::Vec3d Pt_l;// from pointcloud

    Pt_l[0] = +(double)PL_x;
    Pt_l[1] = -(double)PL_y; // e l asse z di profondita , e negativa
    Pt_l[2] = (double)0.0;

    cv::Mat Pt_gm = R_global*cv::Mat(Pt_l);

    cv::Vec3d Pt_g;

    Pt_g[0] = Pt_gm.at<double>(0,0);
    Pt_g[1] = Pt_gm.at<double>(1,0);
    Pt_g[2] = 0;

    Pt_lg[0] = Pt_0[0] + Pt_g[0];
    Pt_lg[1] = Pt_0[1] + Pt_g[1];
    Pt_lg[2] = 0;

    PT_world = cv::Point2d(Pt_lg[0], Pt_lg[1]);
    return PT_world;



}
//FUNZIONE CONVERSIONE QUATERNIONI EULERIANO
T265::EulerAngles T265::Quat_2_euler(Quaternion q){
    EulerAngles angles;

    // roll (x-axis rotation)
    double sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
    double cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
    angles.roll = std::atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = 2 * (q.w * q.y - q.z * q.x);
    if (std::abs(sinp) >= 1)
        angles.pitch = std::copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
        angles.pitch = std::asin(sinp);

    // yaw (z-axis rotation)
    double siny_cosp = 2 * (q.w * q.z + q.x * q.y);
    double cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
    angles.yaw = std::atan2(siny_cosp, cosy_cosp);

    return angles;

}

//FUNZIONE PER GENERARE I MURI ASSOCIATI AI MARKER
void T265::Draw_generate_wall_aruco (cv::Mat fixed_image, cv::Mat map2draw,cv::Point2d left_aruco,cv::Point2d right_aruco, cv::Scalar color){
    cv::circle(map2draw,left_aruco,20,cv::Scalar(0,0,255));
    cv::circle(map2draw,right_aruco,20,cv::Scalar(255,0,0));






    //std::cout<< "punti base muro"<<left_aruco.y << "   "<< left_aruco.x<<std::endl;
    double dist_LR = sqrt(pow(left_aruco.x-right_aruco.x,2) + pow(left_aruco.y - right_aruco.y,2));
    //std::cout <<"marker distance "<<dist_LR <<std::endl;



    int LL = 100;
    int LR = 200;

    double m = ((double)(right_aruco.y-left_aruco.y))/((double)(right_aruco.x-left_aruco.x));

    //double q = (P2_aruco.y-P1_aruco.y) - m*(P2_aruco.x-P1_aruco.x);
    double alpha = atan(m);

    // std::cout<<"delta y :  "<<(right_aruco.y-left_aruco.y)<<"   delta x : "<<right_aruco.x-left_aruco.x<< std::endl;
    //  std::cout<<"m retta :  "<<m<<"   alpha gradi : "<<alpha*180/PI<< std::endl;

    if(dist_LR > 20 && dist_LR <50) {


        double dalta_y = LL*sin(alpha);
        double dalta_x = LL*cos(alpha);
        cv::Point2i P_i = cv::Point2i(left_aruco.x-dalta_x, left_aruco.y-dalta_y );
        double dalta_y_r = LR*sin(alpha);
        double dalta_x_r = LR*cos(alpha);
        cv::Point2i P_f = cv::Point2i(right_aruco.x+dalta_x_r, right_aruco.y+dalta_y_r );
        cv::line(map2draw,P_i,P_f, color,3);

        double M1 = 200 ;



        double theta =  110*PI/180  ;



        //    double theta = 70*PI/180 - (PI/2 - m);

        cv::Point2d P1 = cv::Point2d( P_f.x - (  M1*cos(alpha) * (-cos(theta)) + -sin(alpha)*( -sin(theta))*M1 ) ,  P_f.y  -( M1*(-cos(theta))*sin(alpha) -sin(theta)*M1*cos(alpha) ) );
        cv::line(map2draw,P_f,P1, color,3);



    }




}



double T265::variance(std::vector<double> observations){

    observations = moving_avarege_lenght_control(observations);

    double media = Dmedia(observations);
    int element = 0;

    double somma_scarti_quadratici = 0;
    for(int i = 0; i < observations.size(); i ++){
        if(observations[i] != 0){

            double scarto_quadratico = pow(observations[i]-media,2);

            somma_scarti_quadratici += scarto_quadratico;
            element++;
        }
        else {
            observations.erase(observations.begin() + i);
        }


    }
    double scarto_quadatico_medio = somma_scarti_quadratici/element;
    return scarto_quadatico_medio;






}

double T265::Dmedia(std::vector<double> observations){

    observations = moving_avarege_lenght_control(observations);

    int element = 0;
    double sum = 0;

    for(int i = 0; i < observations.size(); i ++){
        if(observations[i] != 0){
            sum += observations[i];
            element++;


        }
        else {
            observations.erase(observations.begin() + i);
        }

    }
    double media = sum/element;
    return media;



}
double T265::scarto_medio(std::vector<double> observations){

    observations = moving_avarege_lenght_control(observations);

    double media = Dmedia(observations);
    int element = 0;

    double somma_scarti = 0;
    for(int i = 0; i < observations.size(); i ++){
        if(observations[i] != 0){

            double scarto = sqrt(pow(observations[i]-media,2));

            somma_scarti += scarto;
            element++;
        }
        else {
            observations.erase(observations.begin() + i);
        }


    }
    double scarto_medio = somma_scarti/element;
    return scarto_medio;





}
double  T265::distanza_2_punti2D (cv::Point2d p1, cv::Point2d p2){
    double distanza = sqrt(pow(p1.x-p2.x,2) +pow(p1.y-p2.y,2));


    return distanza;
}

double T265::media_mobile(std::vector<double> observations, int n_width){

    while(observations.size() > n_width){
        observations.erase(observations.begin());
    }


    observations = moving_avarege_lenght_control(observations);

    int element = 0;
    double sum = 0;

    for(int i = 0; i < observations.size(); i ++){
        if(observations[i] != 0){
            sum += observations[i];

            element++;



        }
        else {
            observations.erase(observations.begin() + i);
        }

    }
    double media = sum/element;
    return media;


}




//}



std::vector<std::vector<double>> T265::fit_line(cv::Mat image,std::vector<std::vector<double>> XYTHETA_final_depth, std::vector<cv::Point2d> points, std::vector<double> vel_omega, std::vector<double> positions_from_aruco_vector_x, std::vector<double> positions_from_aruco_vector_y, std::vector<double> angles)
{





    if ( points.size() > 0){


        std::vector<double> AR; //vx vy x0 y0

        cv::fitLine(points, AR, CV_DIST_WELSCH, 0, 0.01, 0.01 );





        double theta = -atan(AR[1]/AR[0]);
        //angles.push_back(theta);

        //cv::Point2d robot = cv::Point2d(AR[2],AR[3]);
        ///std::cout<<"theta " <<theta<<std::endl;

        // positions_from_aruco_vector_x.push_back(-robot.x);
        // positions_from_aruco_vector_y.push_back(-robot.y);

        cv::Point2d robot = Trasform_D435LocalP_2_WORLD(theta,0,0,AR[2],AR[3]);

        XYTHETA_final_depth[0].push_back(-robot.x);
        XYTHETA_final_depth[1].push_back(-robot.y);
        XYTHETA_final_depth[2].push_back(theta);





        cv::circle(image,cv::Point2i(-robot.x+map_dim/2, -robot.y+map_dim/2) ,5,cv::Scalar(255,255,0));


        if(pow(vel_omega[0],2) < 1 && pow(vel_omega[1],2) < 1){

            if(pow(vel_omega[2],2) < 1){




                if(XYTHETA_final_depth[0].size() > 5 ){


                    ellipse_static_robot(cv::Scalar(0,0,255),image,XYTHETA_final_depth[0],XYTHETA_final_depth[1],XYTHETA_final_depth[2]);
                }






            }
        }


    }

    return XYTHETA_final_depth;






}
std::vector<double> T265::moving_avarege_lenght_control(std::vector<double> observations){
    int max_length = 250;
    while (observations.size() > max_length ){
        observations.erase(observations.begin());


    }

    return observations;

}





void T265::write_vector_csv_file(std::vector<double> vector, std::string nome ){


    if (vector.size() == 200){
        std::string string;
        string += "/home/bernardo/Scrivania/test/";
        string += nome;
        string += ".csv";
        std::ofstream ofs (string);

        //  std::ofstream ofs ("/home/bernardo/Scrivania/test/"+ std::to_string(nome) + ".csv", std::ofstream::out);

        for(int i = 0 ; i < vector.size(); i++){
            std::string  string_value = std::to_string( vector[i]);
            ofs << string_value;
            ofs<< std::endl;

        }




        ofs.close();


    }

}

void T265::write_vector_csv_file_int(std::vector<int> vector, std::string nome ){


    if (vector.size() == 200){

        std::string string;
        string += "/home/bernardo/Scrivania/test/";
        string += nome;
        string += ".csv";
        std::ofstream ofs (string);

        //  std::ofstream ofs ("/home/bernardo/Scrivania/test/"+ std::to_string(nome) + ".csv", std::ofstream::out);
        for(int i = 0 ; i < vector.size(); i++){
            std::string  string_value = std::to_string( vector[i]);
            ofs << string_value;
            ofs<< std::endl;

        }




        ofs.close();


    }

}



void T265::ellipse_static_robot(cv::Scalar color,cv::Mat image , std::vector<double> positions_from_aruco_vector_x,std::vector<double> positions_from_aruco_vector_y , std::vector<double> angles){


    if (positions_from_aruco_vector_x.size() > 10 ){
        //  std::cout<< " punti stereo vector : "<< positions_from_aruco_vector_x.size() <<std::endl;

        double varx = variance(positions_from_aruco_vector_x);
        double vary = variance(positions_from_aruco_vector_y);
        double mediax = Dmedia(positions_from_aruco_vector_x);
        double mediay = Dmedia(positions_from_aruco_vector_y);
        double media_angles = Dmedia(angles);

        //   std::cout<<"xm ym varx,var y "<< mediax<<"  "<< mediay<<"  "<<varx<<"  "<<vary<<std::endl;

        if(varx > 1 && vary > 1){

            cv::ellipse( image,cv::Point2i(mediax ,  mediay ), cv::Size(varx,vary), media_angles*180/PI,0 , 360, color, 2,8);
        }

        else {
            cv::ellipse( image,cv::Point2i(mediax ,  mediay ), cv::Size(1,1), media_angles*180/PI,0 , 360, color, 2,8);

        }


    }
}




void T265::fusion_dist_percorsa(cv::Mat image , std::vector<double> x_aruco, std::vector<double> y_aruco,  std::vector<double> theta_aruco, std::vector<double> x_t265,std::vector<double> y_t265, std::vector<double> theta_t265){






}


std::vector<std::vector<double>> T265::manage_XYtheta_from_stereo_aruco(cv::Mat image_fixed , std::vector<std::vector<double>> RGB_POSE_TRANS ,cv::Mat image, cv::Point2d pose, double angle,std::vector<double> X_pose, std::vector<double> Y_pose, std::vector<double> asset_theta, std::vector<std::vector<double>> XYtheta_vector_stereo){




    // cv::Point2d robot_stereo = Trasform_D435LocalP_2_WORLD( angle, 0,0,pose.x, pose.y  );
    cv::Point2d robot_stereo = cv::Point2d(-pose.x,-pose.y);
    // std::cout<<"robot global " << pose <<std::endl;
    cv::line(image,cv::Point(map_dim/2-20,map_dim/2),cv::Point(map_dim/2+20,map_dim/2),cv::Scalar(0,0,255),5 );



    X_pose.push_back(robot_stereo.x);
    Y_pose.push_back(robot_stereo.y);
    asset_theta.push_back(angle);





    cv::Point2d robot_respect_aruco= Trasform_D435LocalP_2_WORLD(angle, 0,0,robot_stereo.x,robot_stereo.y);

    RGB_POSE_TRANS[0].push_back(robot_respect_aruco.x+map_dim/2);
    RGB_POSE_TRANS[1].push_back(robot_respect_aruco.y+map_dim/2);
    RGB_POSE_TRANS[2].push_back(angle);


    /*
    cv::circle(image,cv::Point2i(robot_respect_aruco.x+map_dim/2, robot_respect_aruco.y+map_dim/2) ,10,cv::Scalar(255,255,0));
    cv::circle(image_fixed,cv::Point2i(robot_respect_aruco.x+map_dim/2, robot_respect_aruco.y+map_dim/2) ,2,cv::Scalar(255,0,0));
    cv::line(image,cv::Point2i(robot_respect_aruco.x+map_dim/2, robot_respect_aruco.y+map_dim/2), cv::Point2i(robot_respect_aruco.x+map_dim/2 - 20*sin(angle), robot_respect_aruco.y+map_dim/2 - 20*cos(angle)),cv::Scalar(0,0,255),2 );
*/






    return RGB_POSE_TRANS;

}




std::vector<cv::Point2d> T265::sensor_fusion_static_D435(cv::Mat image , std::vector<double> observation_x,std::vector<double> observation_y , std::vector<double> observation_zx,std::vector<double> observation_zy){

    double x_med = Dmedia(observation_x);
    double y_med = Dmedia(observation_y);
    double zx_med = Dmedia(observation_zx);
    double zy_med = Dmedia(observation_zy);

    cv::Vec2d X(x_med,y_med);
    cv::Vec2d Z(zx_med,zy_med);


    cv::Mat cov_X = covariance_matrix_2(observation_x,observation_y);
    cv::Mat cov_Z = covariance_matrix_2(observation_zx, observation_zy);


    cv::Mat sum_CX_CZ = cov_X + cov_Z;

    cv::Mat inv_sum = sum_CX_CZ.inv();

    cv::Mat COV_out_mat = cov_Z * inv_sum * cov_X;


    cv::Mat X_med_out =  ( cov_X * inv_sum * cv::Mat(Z) )+ ( cov_Z * inv_sum * cv::Mat(X) );



    std::vector<cv::Point2d> punti_fusi ;

    cv::Point2d punto_fuso = cv::Point2d(X_med_out);


    cv::circle(image,cv::Point2d(punto_fuso.x+map_dim/2 , punto_fuso.y +map_dim/2), 10, cv::Scalar(0,100,100),2);


    punti_fusi.push_back(punto_fuso);
    cv::ellipse( image,cv::Point2d(punto_fuso.x + map_dim/2,  punto_fuso.y + map_dim/2), cv::Size(COV_out_mat.at<double>(0,0),COV_out_mat.at<double>(1,1)), 0, 0,360, cv::Scalar(100,100,100), 2,8);




    //// cv::ellipse( image,cv::Point2i(-mediax + map_dim/2, - mediay + map_dim/2), cv::Size(varx,vary), 0, media_angles,360, color, 2,8);



    return punti_fusi;




}





cv::Mat T265::covariance_matrix_2(std::vector<double> observation_x,std::vector<double> observation_y ){



    cv::Mat covariance_mat;

    covariance_mat = (cv::Mat_<double>(2,2) <<  variance(observation_x),  covariance(observation_x,observation_y), covariance(observation_x,observation_y), variance(observation_y));
    return covariance_mat;



}

cv::Mat T265::covariance_matrix_3(std::vector<double> observation_x,std::vector<double> observation_y , std::vector<double> observation_z){



    cv::Mat covariance_mat;

    covariance_mat = (cv::Mat_<double>(3,3) <<  variance(observation_x),  covariance(observation_x,observation_y), covariance(observation_x,observation_z),covariance(observation_x,observation_y), variance(observation_y),covariance(observation_y,observation_z),covariance(observation_x,observation_z),covariance(observation_y,observation_z), variance(observation_z) );
    return covariance_mat;



}


double T265::covariance (std::vector<double> observation_x,std::vector<double> observation_y ){




    double media_x = Dmedia(observation_x);
    double media_y = Dmedia(observation_y);
    int element = 0;

    double somma_covar = 0;
    for(int i = 0; i < observation_x.size(); i ++){
        if(observation_x[i] != 0 && observation_y[i] != 0){

            double scarto_quadratico = (observation_x[i]-media_x)*(observation_y[i]-media_y);

            somma_covar += scarto_quadratico;
            element++;
        }
        else {
            observation_x.erase(observation_x.begin() + i);
            observation_y.erase(observation_y.begin() + i);
        }


    }
    double covar = somma_covar/element;
    return covar;






}


cv::Vec3d T265::sensor_fusion_static_D435_3(cv::Mat image , std::vector<double> observation_x,std::vector<double> observation_y ,std::vector<double> observation_z, std::vector<double> observation_zx,std::vector<double> observation_zy,std::vector<double> observation_zz){

    double x_med = Dmedia(observation_x);
    double y_med = Dmedia(observation_y);
    double z_med = Dmedia(observation_z);
    double zx_med = Dmedia(observation_zx);
    double zy_med = Dmedia(observation_zy);
    double zz_med = Dmedia(observation_zz);

    //  std::cout<<"observation size"<< observation_zx.size() <<"  "<< observation_zy.size() <<std::endl;

    cv::Vec3d X(x_med,y_med,z_med);
    cv::Vec3d Z(zx_med,zy_med, zz_med);


    cv::Mat cov_X = covariance_matrix_3(observation_x,observation_y,observation_z);
    cv::Mat cov_Z = covariance_matrix_3(observation_zx, observation_zy,observation_zz);


    cv::Mat sum_CX_CZ = cov_X + cov_Z;

    cv::Mat inv_sum = sum_CX_CZ.inv();

    cv::Mat COV_out_mat = cov_Z * inv_sum * cov_X;


    cv::Mat X_med_out =  ( cov_X * inv_sum * cv::Mat(Z) )+ ( cov_Z * inv_sum * cv::Mat(X) );



    std::vector<cv::Vec3d> punti_fusi ;

    cv::Vec3d punto_fuso = cv::Vec3d(X_med_out);


    cv::circle(image,cv::Point2d(punto_fuso[0]+map_dim/2 , punto_fuso[1] +map_dim/2), 5, cv::Scalar(0,100,100),2);


    punti_fusi.push_back(punto_fuso);
    if(observation_x.size()>5){

        cv::ellipse( image,cv::Point2d(punto_fuso[0]+map_dim/2 ,  punto_fuso[1] +map_dim/2), cv::Size(COV_out_mat.at<double>(0,0),COV_out_mat.at<double>(1,1)), punto_fuso[2]*180/PI,360, 0, cv::Scalar(255,100,0), 2,8);

    }






    return punto_fuso;




}


std::vector<std::vector<double>> T265::trajectory_global_implementation(cv::Mat image_fixed,  cv::Vec3d last_fused_position, std::vector<std::vector<double>> T265_info, std::vector<std::vector<double>> T265_global){






    cv::Point2d pt = cv::Point2d (T265_info[0].back(),T265_info[1].back()  );

    cv::Point2d pt_global = Trasform_D435LocalP_2_WORLD(last_fused_position[2], (int)last_fused_position[0],(int)last_fused_position[1],pt.x,pt.y);

    T265_global[0].push_back(pt_global.x);
    T265_global[1].push_back(pt_global.y);
    T265_global[2].push_back(last_fused_position[2]);

    cv::Point2d pt_last = cv::Point2d(T265_global[0].back(), T265_global[1].back());
    cv::Point2d pt_penultimo = cv::Point2d(T265_global[0][T265_global[0].size() -1],T265_global[1][T265_global[1].size() -1]);


    //cv::circle(image_fixed,pt_global,2,cv::Scalar(0,0,255));
    cv::line(image_fixed,pt_penultimo,pt_last,cv::Scalar(0,0,255),1);




    return T265_global;
}



cv::Vec3d T265::fusion_T265_D435_trj ( std::vector<std::vector<double>> T265_trj_transf, std::vector<std::vector<double>> D435_global_withT265,cv::Vec3d startP, cv::Mat image,cv::Mat fixed_image, std::vector<std::vector<double>> raw_D435_pose_data, std::vector<std::vector<double>> velocity_current_data,  std::vector<std::vector<double>> T265_odometry_pose, std::vector<std::vector<double>> fused_trj_final,std::vector<std::vector<double>> D435_local_data){
    cv::Mat trj;
    image.copyTo(trj);
    //prendo gli ultimi tot valori anche tanti
    double vx = media_mobile(velocity_current_data[0],100);
    double vy = media_mobile(velocity_current_data[1],100);
    double vtheta = media_mobile(velocity_current_data[2],100);
    if (raw_D435_pose_data[0].size() > 2){

        cv::circle(image,cv::Point2d(raw_D435_pose_data[0].back(),raw_D435_pose_data[1].back()),1,cv::Scalar(255,100,50),1);
        cv::line(image,cv::Point2d(raw_D435_pose_data[0].back(),raw_D435_pose_data[1].back()),cv::Point2d(raw_D435_pose_data[0].back() - 10*sin(raw_D435_pose_data[2].back()),raw_D435_pose_data[1].back() - 10*cos(raw_D435_pose_data[2].back())),cv::Scalar(0,0,255),1);
        //std::cout<<"angle :"<< raw_D435_pose_data[1].back() <<std::endl;


        if(startP[0] == 0){
            if(abs(vx) < 0.1 && abs(vy) < 0.1 && abs(vtheta) < 0.1){ //si attiva a basse velocità


                std::cout<<"creating start point, start : "<<startP<<std::endl;
                double x_start = media_mobile(raw_D435_pose_data[0],100);
                double  y_start = media_mobile(raw_D435_pose_data[1],100);
                double  theta_start = media_mobile(raw_D435_pose_data[2],100);
                sigma_theta = variance(raw_D435_pose_data[2]);

                if (raw_D435_pose_data[0].size() > 70){
                    startP[0] = x_start;
                    startP[1] = y_start;
                    startP[2] = theta_start;

                    cv::circle(fixed_image,cv::Point2d(startP[0],startP[1]),20,cv::Scalar(255,255,0),4);

                    cv::putText(fixed_image,"STARTING POINT FOUND",cv::Point2i( map_dim - 300, map_dim-400),cv::FONT_HERSHEY_COMPLEX_SMALL,1, cv::Scalar(0,255,255));
                    cv::line(image,cv::Point2d(startP[0],startP[1]),cv::Point2d(startP[0] - 60*sin(startP[2]),startP[1] - 60*cos(startP[2])),cv::Scalar(0,100,255),2);

                }
                std::cout<<"creating start point, start : "<<startP<<std::endl;

            }


        }

        else {

            if (abs(startP[0]) > 1){ //capisco se ho assegnato unn punto di start
                //fusion

                for (int i = 0 ; i < T265_odometry_pose[0].size(); i++){

                    cv::Point2d trj_t65_trans = Trasform_D435LocalP_2_WORLD(startP[2],startP[0],startP[1],T265_odometry_pose[0][i],T265_odometry_pose[1][i]);
                    cv::Point2d trj_t65_trans_max = Trasform_D435LocalP_2_WORLD(startP[2]+ sqrt(sigma_theta),startP[0],startP[1],T265_odometry_pose[0][i],T265_odometry_pose[1][i]);
                    cv::Point2d trj_t65_trans_min = Trasform_D435LocalP_2_WORLD(startP[2]- sqrt(sigma_theta),startP[0],startP[1],T265_odometry_pose[0][i],T265_odometry_pose[1][i]);




                    T265_odometry_pose[2][i] = T265_odometry_pose[2][i]+startP[2];
                    if(D435_local_data[1].size() > 10){
                        if ( abs(D435_local_data[1][i]) < 30 )    {
                            D435_local_data[1][i] = D435_local_data[1][i-1];
                            D435_local_data[0][i] = D435_local_data[0][i-1];

                        }
                    }

                    cv::Point2d D435_global_thetaVO = Trasform_D435LocalP_2_WORLD(T265_odometry_pose[2][i],map_dim/2,map_dim/2,D435_local_data[1][i],-D435_local_data[0][i]);


                    //std::cout<< "d435 local: "<< D435_local_data[0][i] <<"  "<<D435_local_data[1][i]<<"angle : "<<T265_odometry_pose[2][i]<<std::endl;



                    T265_trj_transf[0].push_back(trj_t65_trans.x);
                    T265_trj_transf[1].push_back(trj_t65_trans.y);
                    T265_trj_transf[2].push_back(T265_odometry_pose[2][i]);

                    D435_global_withT265[0].push_back(D435_global_thetaVO.x);
                    D435_global_withT265[1].push_back(D435_global_thetaVO.y);
                    D435_global_withT265[2].push_back(T265_odometry_pose[2][i]);

                }

                cv::line(image,cv::Point2d(T265_trj_transf[0].back(),T265_trj_transf[1].back()),cv::Point2d(T265_trj_transf[0].back()- 30*sin(T265_trj_transf[2].back()),T265_trj_transf[1].back()- 30*cos(T265_trj_transf[2].back())),cv::Scalar(0,0,255),1);
                cv::circle(image,cv::Point2d(T265_trj_transf[0].back(),T265_trj_transf[1].back()) ,1,cv::Scalar(0,0,255),1);
                cv::line(image,cv::Point2d(D435_global_withT265[0].back(),D435_global_withT265[1].back()),cv::Point2d(D435_global_withT265[0].back()- 30*sin(D435_global_withT265[2].back()),D435_global_withT265[1].back()- 30*cos(D435_global_withT265[2].back())),cv::Scalar(255,0,255),1);
                cv::circle(image,cv::Point2d(D435_global_withT265[0].back(),D435_global_withT265[1].back()) ,1,cv::Scalar(255,0,255),1);
                std::cout << "d435!" <<cv::Point2d(T265_trj_transf[0].back(),T265_trj_transf[1].back()) << std::endl;





                for(int i= 0; i < T265_trj_transf[0].size(); i++){

                    if (i > 70){

                        cv::circle(trj,cv::Point2d(T265_trj_transf[0][i],T265_trj_transf[1][i]) ,1,cv::Scalar(0,0,255),1);
                        cv::circle(trj,cv::Point2d(D435_global_withT265[0][i],D435_global_withT265[1][i]) ,1,cv::Scalar(255,0,0),1);
                    }




                }



                cv::Mat trj1;
                // cv::resize(trj, trj1, cv::Size(), 0.5, 0.5);
                cv::Rect roi1(800, 800, 400,400);
                cv::Mat zoomed1 = trj(roi1);
                // cv::resize(zoomed1, zoomed1, cv::Size(), 3, 3);
                //cv::imshow("traj", zoomed1);

                cv::waitKey(1);

            }

            else {
                std::cout << "punto di start non inizializzato-traiettoria incompatibile, stabilizza il robot!" << std::endl;
            }



        }


    }





    return startP;

}


cv::Vec3d T265::uncertanty_dinamic_generator_D435(double vx,double vy, double vtheta){

    double ax = 0.2175;
    double bx = 0.66;
    double ay = 20.47;
    double by = 0.7538;
    double atheta = 0.01789;
    double btheta = 0.6017;

    double sigmax = ax * (pow(vy,bx));
    double sigmay = ay * (pow(vx,by));
    double sigmatheta = ay * (pow(vtheta,btheta));

    cv::Vec3d uncertanty;
    uncertanty[0] = sigmax;
    uncertanty[1] = sigmay;
    uncertanty[2] = sigmatheta;



    return uncertanty;

}







