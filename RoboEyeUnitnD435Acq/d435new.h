#ifndef D435NEW_H
#define D435NEW_H
#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>
#include <GLFW/glfw3.h>




class D435new
{
public:
    D435new();
    rs2::colorizer color_map;
    rs2::rates_printer printer;
    rs2::pipeline pipe;
    rs2::frameset frames;
    cv::Mat depthMat;
    rs2::frame frame;
    rs2::config cfg;


};

#endif // D435NEW_H
